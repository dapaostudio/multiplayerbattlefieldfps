﻿using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Events;

//步兵状态挂载的基类，管理控制器、武器等一切跟步兵有关的东西，Player会引用本类来管理FootState时的控制
public class Soldier : MonoBehaviour
{
    //对Player的上级引用，拖拽绑定
    public Player Player;
    //士兵移动状态
    public SoldierStatus Status;
    //状态-移动速度字典
    public Dictionary<SoldierStatus, float> SoldierSpeedDict = new Dictionary<SoldierStatus, float>()
    { { SoldierStatus.Walk, 4f } ,{ SoldierStatus.Sprint,8f},{ SoldierStatus.Crouch,2f},{ SoldierStatus.Lie,1f},{ SoldierStatus.CantMove,0f} }; 
    //最大跳跃高度
    public float JumpHight = 3f;
    //角色身高与蹲下高度
    public float OriginHight = 2f;
    public float CrouchHight = 1f;
    //上下视角最小与最大值
    public Vector2 MaxminViewAngle;
    //步兵状态虚拟摄像机（视角、瞄准）
    public CinemachineVirtualCamera PlayerViewCamera;
    public Camera PlayerGunCamera;
    //步兵控制器（是否是必须的）
    public SoldierControl SoldierControl;
    //士兵第一人称手臂容器，负责视角俯仰
    public Transform SoldierFPHolder;
    //士兵当前水平移动速度（用于播放脚步声）
    public float SoldierSpeed;
    //士兵脚部位置（用于播放脚步声）
    public Transform SoldierFoot;
    //士兵震屏Transform
    public Transform CameraSpringTransform;
    //士兵当前Animator
    public Animator CurrentAnimation;
    //士兵Data
    //士兵HitBox

    void Start()
    {
        //初始化视角限制
        MaxminViewAngle = new Vector2(-65f, 65f);
    }

    void Update()
    {

    }
}

public enum SoldierStatus
{
    Walk,
    Sprint,
    Crouch,
    Lie,
    CantMove
}
