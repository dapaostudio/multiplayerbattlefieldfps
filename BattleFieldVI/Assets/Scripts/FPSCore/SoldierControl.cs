﻿using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;

//使用CharacterController来实现的角色第一人称控制器，包含鼠标和键盘操作
public class SoldierControl : MonoBehaviour
{
    //步兵角色控制器
    CharacterController characterController;
    //对士兵核心类的引用
    Soldier soldier;
    //移动输入与视角输入
    Vector2 moveInput;
    Vector2 viewInput;
    //当前视角朝向
    Vector3 viewRotation;
    //移动方向与移动速度（矢量）
    Vector3 moveDirection;
    Vector3 moveVelocity;
    //角色Transform
    Transform characterTransform;
    //重力
    const float GRAVITY = 9.8f;
    //跳跃与下蹲标志，稍微有点蠢，但暂时不知道怎么改，后面再说吧
    bool isJump = false;
    bool isCrouch = false;
    bool isLie;
    //下蹲协程
    Coroutine crouchCoroutine;
    //地面监测射线长度
    float isGroundHeight = 0.1f;
    //地面监测射线结果
    RaycastHit groundHit;

    void Start()
    {
        soldier = GetComponent<Soldier>();
        characterController = GetComponent<CharacterController>();
        characterTransform = GetComponent<Transform>();
    }

    void OnEnable()
    {
        Start();
    }

    void FixedUpdate()
    {
        Move();
    }
    void Update()
    {
        ViewControl();
    }
    void LateUpdate()
    {
        //在完成相机旋转计算后再在每一帧逻辑的最后应用相机的旋转
        ApplyViewRotation();
    }

    // 从角色底部向下发射射线,如果射线接触到的物体的标签不是"Player"，返回true,否则返回false
    bool InGround()
    {
        Vector3 CharacterBottom = new Vector3(transform.position.x, transform.position.y - characterController.height / 2, transform.position.z);
        //Debug.DrawLine(CharacterBottom, new Vector3(CharacterBottom.x, CharacterBottom.y-isGroundHeight, CharacterBottom.z), Color.red, 0.25f);
        if (Physics.Raycast(CharacterBottom, -Vector3.up, out groundHit, isGroundHeight))
            if (!groundHit.collider.CompareTag("Player"))
                return true;
        return false;
    }

    //设置与移动相关的动画参数
    void SetAnimatorParameter(float currentVelocity)
    {
        if(soldier.CurrentAnimation != null) 
        {
            soldier.CurrentAnimation.SetFloat("Velocity", currentVelocity, 0.25f, Time.deltaTime);
        }
    }

    void ViewControl() 
    {
        //计算视角各轴旋转值
        viewRotation.x -= viewInput.y * GameSettings.ViewSensitive;
        viewRotation.y += viewInput.x * GameSettings.ViewSensitive;
    }

    void ApplyViewRotation()
    {
        //摄像机俯仰限位
        viewRotation.x = Mathf.Clamp(viewRotation.x, soldier.MaxminViewAngle.x, soldier.MaxminViewAngle.y);
        //视角旋转
        soldier.SoldierFPHolder.rotation = Quaternion.Euler(viewRotation.x, viewRotation.y, 0);
        soldier.transform.rotation = Quaternion.Euler(0, viewRotation.y, 0);
    }

    void Move()
    {
        //如果不能动就不动
        if (soldier.Status == SoldierStatus.CantMove) return;
        //计算Y轴速度
        if (characterController.isGrounded) moveVelocity.y = 0;
        else moveVelocity.y -= GRAVITY * Time.fixedDeltaTime;
        //计算移动方向
        moveDirection = characterTransform.TransformDirection(new Vector3(moveInput.x, 0, moveInput.y));
        if (moveDirection.magnitude > 1f)
            moveDirection.Normalize();
        //计算跳跃速度速度
        moveDirection.y = isJump ? Mathf.Sqrt(2 * soldier.JumpHight * GRAVITY) : moveVelocity.y;
        isJump = false;
        //计算移动速度
        float currentSpeed = soldier.SoldierSpeedDict[soldier.Status];
        moveVelocity = new Vector3(moveDirection.x * currentSpeed, moveDirection.y, moveDirection.z * currentSpeed);
        //更新移动动画参数
        float currentVelocity = new Vector3(moveVelocity.x, 0, moveVelocity.z).magnitude;
        SetAnimatorParameter(currentVelocity);
        //更新士兵水平移速
        soldier.SoldierSpeed = currentVelocity;
        //执行移动
        characterController.Move(moveVelocity * Time.fixedDeltaTime);
    }

    //获取武器横向与纵向后坐力带来的偏移，由武器来通过WeaponManager访问Soldier每一帧来调用，差值工作交给Firearm，等会试试能不能跑，主要担心他有没有在LateUpdate之前
    /// <summary>
    /// 控制武器的水平后座与垂直后座
    /// </summary>
    /// <param name="recoilHorizontalOffset">水平后坐力</param>
    /// <param name="recoilVerticalOffset">垂直后坐力</param>
    public void GetRecoilOffset(float recoilHorizontalOffset,float recoilVerticalOffset)
    {
        //X轴是俯仰，即垂直后座
        viewRotation.x -= recoilVerticalOffset;
        //Y轴是偏航，即水平后座
        viewRotation.y += Random.Range(0, 100) % 2 == 0 ? recoilHorizontalOffset : -recoilHorizontalOffset;
    }
    public void GetMoveInput(Vector2 _moveInput)
    {
        moveInput = _moveInput;
    }

    public void GetViewInput(Vector2 _viewInput)
    {
        viewInput = _viewInput;
        //Mouse Delta的返回结果与GetAxis有较大区别，暂时解决不了这一问题，所以先用回Input.GetAxis，留一个TODO吧
        viewInput.x = Input.GetAxis("Mouse X");
        viewInput.y = Input.GetAxis("Mouse Y");
    }

    public void Sprint(bool OnOff)
    {
        //只有行走状态可以与冲刺状态互相切换
        if (soldier.Status != SoldierStatus.Walk && soldier.Status != SoldierStatus.Sprint) return;
        //在空中不可冲刺，未考虑开伞状态
        if (!InGround()) return;
        //切换冲刺与行走状态
        if (OnOff)
        {
            soldier.Status = SoldierStatus.Sprint;
        }
        else
        {
            soldier.Status = SoldierStatus.Walk;
        }
    }

    public void Crouch(bool OnOff)
    {
        if (OnOff)
        {
            //下蹲状态
            soldier.Status = SoldierStatus.Crouch;
            //只蹲一次
            if(isCrouch == false)
            {
                if (crouchCoroutine != null)
                {
                    StopCoroutine(crouchCoroutine);
                }
                crouchCoroutine = StartCoroutine(DoCrouch(OnOff));
                isCrouch = true;
            }
        }
        else
        {
            //站起
            soldier.Status = SoldierStatus.Walk;
            //只站一次
            if(isCrouch == true)
            {
                if (crouchCoroutine != null)
                {
                    StopCoroutine(crouchCoroutine);
                }
                crouchCoroutine = StartCoroutine(DoCrouch(OnOff));
                isCrouch = false;
            }
        }
    }

    public void SwitchCrouch()
    {
        Crouch(!isCrouch);
        //切换成相反状态
    }
    // 需要动作，先不做，可能要旋转CharacterController？
    public void SwitchLie()
    {
        //soldier.Status = SoldierStatus.Lie;
    }

    //跳跃按钮，赋予一个向上的速度
    public void Jump()
    {
        if(InGround()) 
        {
            isJump = true;
        }
    }

    IEnumerator DoCrouch(bool OnOff)
    {
        float curruntHight = 0f;
        float _targethight = OnOff ? soldier.CrouchHight : soldier.OriginHight;
        while (Mathf.Abs(characterController.height - _targethight) > 0.05f) 
        {
            yield return null;
            characterController.height = Mathf.SmoothDamp(characterController.height, _targethight, ref curruntHight, Time.deltaTime * 5);
        }
        characterController.height = _targethight;
    }
}
