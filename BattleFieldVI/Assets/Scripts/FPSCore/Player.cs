using Assets.Scripts.Weapon;
using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;

public class Player : MonoBehaviour
{
    //角色战绩姓名生命值等信息
    public string PlayerName;
    public string PlayerID;
    public int PlayerScore, PlayerKillCount, PlayerDeathTime, PlayerSupportCount, PlayerSaveCount;
    public Vector3 PlayerPosition;
    //玩家阵营
    public Faction PlayerFaction;
    //当前状态引用
    public IPlayerState CurrentState;
    //当前是否为肉身状态
    public bool IsCurrentOnlyHuman;
    //震屏组件
    public CameraSpring CameraSpring;
    //InputSystem引用
    public PlayerInput PlayerInput;
    //角色实例引用
    public GameObject Soldier;
    //载具引用
    public GameObject CurrentAvatar;
    //步兵状态虚拟摄像机（视角、瞄准）
    public CinemachineVirtualCamera PlayerViewCamera;
    public Camera PlayerGunCamera;
    //公用载具虚拟摄像机（第一人称、第三人称、第一人称自由、第三人称自由、瞄准）
    public CinemachineVirtualCamera Avatar1stViewCamera, Avatar3rdViewCamera;
    public CinemachineFreeLook Avatar1stFreeLookCamera, Avatar3rdFreeLookCamera;
    public Camera AvatarGunCamera;
    //武器管理器
    public WeaponManager WeaponManager;
    //交互事件绑定
    public UnityAction<GameObject> InteractionEvent;
    //临时的载具(player里需要做交互事件的绑定)
    public GameObject TempVehicle;

    void Awake()
    {
        PlayerInput = new PlayerInput();
        EnterSoldier();
        InteractionEvent += EnterVehicle;
    }
    void Start()
    {

    }

    void Update()
    {
        if(CurrentAvatar == null)
        {
            PlayerPosition = Soldier.transform.position;
        }
        else
        {
            PlayerPosition = CurrentAvatar.transform.position;
        }
    }

    //进入步兵
    public void EnterSoldier()
    {
        ChangeState(new SoldierState());
        //设置当前是否为肉身State
        IsCurrentOnlyHuman = true;
        CameraSpring.SetSpringTransform(Soldier.GetComponent<Soldier>().CameraSpringTransform);
        //要想办法获取摄像机
    }
    //回到步兵
    public void ResumeSoldier(GameObject AvatarGameObject)
    {
        //移动或显示Soldier
        Soldier.transform.position = AvatarGameObject.transform.position;
        Soldier.SetActive(true);
        //将CurrentAvatar赋值为空
        CurrentAvatar = null;
        //更改State为士兵State
        ChangeState(new SoldierState());
        //设置当前是否为肉身State
        IsCurrentOnlyHuman = true;
        //设置震屏Transform
        CameraSpring.SetSpringTransform(Soldier.GetComponent<Soldier>().CameraSpringTransform);
    }
    //进入载具驾驶位
    public void EnterVehicle(GameObject VehicleGameObject)
    {
        Debug.Log("进入载具了");
        //传入Tag为载具的Object
        //将CurrentAvatar赋值为目标GameObject
        CurrentAvatar = VehicleGameObject;
        //移动或隐藏Soldier
        Soldier.SetActive(false);
        //更改State为载具State
        ChangeState(new VehicleState());
        //调整摄像机状态与权重
        SetCAM();
        //设置当前是否为肉身State
        IsCurrentOnlyHuman = false;
    }
    //进入飞机
    public void EnterAircraft()
    {

    }
    //进入武器站
    public void EnterStation()
    {

    }
    //进入挂件位置
    public void EnterCrew()
    {

    }
    //进入聊天，不能改变Avatar和Object，只暂时屏蔽输入（可以用Enable和Disable？）
    public void EnterChat()
    {

    }
    //进入菜单，不能改变Avatar和Object，只暂时屏蔽输入，后续想想怎么处理TODO
    public void EnterMenu()
    {
        //只改State
    }
    //切换状态
    void ChangeState(IPlayerState newState)
    {
        if (CurrentState != null)
        {
            CurrentState.ExitState();
        }

        CurrentState = newState;
        CurrentState.Player = this;
        CurrentState.EnterState();
    }

    //切换状态时初始化虚拟摄像机权重
    public void SetCAM()
    {
        Avatar3rdViewCamera.Priority = 20;
    }

    public void CloseCAM()
    {
        Avatar3rdViewCamera.Priority = 0;
    }
}

//阵营，用于标记炮弹和玩家
public enum Faction
{
    TeamA,
    TeamB,
    None
}
