﻿using System.Collections;
using UnityEngine;

//用于做FPS中的区域伤害倍率的脚本，挂载在各个HitBox下
public class BodyPart : MonoBehaviour
{
    //所属角色名
    public string PlayerName;
    //装甲类型
    public ArmorType ArmorType;
    //HitBox类型
    public PartType PartType;
    //HitBox阵营
    public Faction PartFaction;
    //载具HitBox部位
    public VehiclePart VehiclePart;

}
//装甲类型
public enum ArmorType
{
    Human,
    Light,
    Heavy
}
//部位类型，前三个为步兵部位，后三个为载具部位
public enum PartType
{
    Head,
    Body,
    Limbs,
    Core,
    Normal,
    Protected,
}
//载具部件，引擎影响速度和加速，炮塔影响炮塔旋转，轮子影响转向
public enum VehiclePart
{
    None,
    Engine,
    Turret,
    Wheel,
}