using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//载具的控制类，使用CharacterController来实现载具的移动和转向，后续使用网络刚体来做物理驾驶
public class VehicleControl : MonoBehaviour
{
    //对载具核心类的引用
    Vehicle vehicle;
    //水平与垂直轴的输入
    float horizontal, vertical;
    //视角输入
    Vector2 viewInput;
    //当前视角朝向
    Vector3 viewRotation;
    //移动方向与移动速度（矢量）
    Vector3 moveDirection;
    Vector3 moveVelocity;
    //载具控制器
    CharacterController vehicleController;
    //载具Transform
    Transform vehicleTransform;
    //重力
    const float GRAVITY = 9.8f;

    void Start()
    {
        vehicle = GetComponent<Vehicle>();
        vehicleController = GetComponent<CharacterController>();
        vehicleTransform = GetComponent<Transform>();
    }


    void Update()
    {
        ViewControl();
    }

    void FixedUpdate()
    {
        Move();
    }

    void ViewControl()
    {
        //计算视角各轴旋转值
        viewRotation.x -= viewInput.y * GameSettings.ViewSensitive;
        viewRotation.y += viewInput.x * GameSettings.ViewSensitive;
        //摄像机俯仰限位

        //视角旋转
        vehicle.Vehicle3rdViewCamera.transform.rotation = Quaternion.Euler(viewRotation.x, viewRotation.y, 0);
        vehicle.VehicleTurret.transform.rotation = Quaternion.Euler(0, viewRotation.y, 0);
    }

    void Move()
    {
        //计算移动方向
        moveDirection = vehicleTransform.TransformDirection(new Vector3(horizontal, 0, vertical));
        if (moveDirection.magnitude > 1f)
            moveDirection.Normalize();
        //计算移动速度
        float currentSpeed = vehicle.VehicleSpeed;
        moveVelocity = new Vector3(moveDirection.x * currentSpeed, moveDirection.y, moveDirection.z * currentSpeed);
        //计算Y轴速度
        moveDirection.y -= GRAVITY * Time.fixedDeltaTime;
        //执行移动
        vehicleController.Move(moveVelocity * Time.fixedDeltaTime);
    }

    //获取转向输入
    public void GetHorizontalInput(float _horizontal)
    {
        horizontal = _horizontal;
    }
    //获取油门输入
    public void GetVerticalInput(float _vertical) 
    {
        vertical = _vertical;
    }
    //获取视角输入
    public void GetViewInput(Vector2 _viewInput)
    {
        viewInput = _viewInput;
        //Mouse Delta的返回结果与GetAxis有较大区别，暂时解决不了这一问题，所以先用回Input.GetAxis，留一个TODO吧
        viewInput.x = Input.GetAxis("Mouse X");
        viewInput.y = Input.GetAxis("Mouse Y");
    }

}
