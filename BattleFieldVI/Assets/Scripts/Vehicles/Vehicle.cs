using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vehicle : MonoBehaviour
{
    //载具第一人称摄像机、第三人称摄像机、自由视角摄像机、瞄准摄像机
    public CinemachineVirtualCamera Vehicle1stViewCamera, Vehicle3rdViewCamera;
    public CinemachineFreeLook Vehicle1stFreeLookCamera, Vehicle3rdFreeLookCamera;
    public Camera VehicleGunCamera;
    //载具炮塔
    public GameObject VehicleTurret;
    //载具位置Transform列表
    public List<Transform> VehicleSeat;
    //载具速度（临时）
    public float VehicleSpeed;
    //载具控制器（是否必须）
    public VehicleControl VehicleControl;
    //载具武器
    //载具Data
    //载具HitBox
    //载具摧毁状态预制体

    void Start()
    {
        
    }


    void Update()
    {
        
    }
}
