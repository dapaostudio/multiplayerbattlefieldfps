using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class EventManager
{
    //事件中心唯一实例
    private static EventManager instance;
    //保证事件中心为单例
    public static EventManager Instance { get { if (instance == null) { instance = new EventManager(); } return instance; } }

    //使用字典存储所有事件
    private Dictionary<string, IEventInfo> _eventDict = new Dictionary<string, IEventInfo>();

    //订阅无参事件消息
    public void AddEventListener(string name, UnityAction action)
    {
        //旧事件
        if (_eventDict.ContainsKey(name))
            (_eventDict[name] as EventInfo).actions += action;
        //新事件
        else
            _eventDict.Add(name, new EventInfo(action));
    }
    //订阅有参事件消息
    public void AddEventListener<T>(string name, UnityAction<T> action)
    {
        //旧事件
        if (_eventDict.ContainsKey(name))
            (_eventDict[name] as EventInfo<T>).actions += action;
        //新事件
        else
            _eventDict.Add(name, new EventInfo<T>(action));
    }

    //通知无参事件消息
    public void EventTrigger(string name)
    {
        if (_eventDict.ContainsKey(name))
            if ((_eventDict[name] as EventInfo).actions != null)
                (_eventDict[name] as EventInfo).actions.Invoke();
    }
    //通知有参事件消息
    public void EventTrigger<T>(string name, T info)
    {
        if (_eventDict.ContainsKey(name))
            if ((_eventDict[name] as EventInfo<T>).actions != null)
                (_eventDict[name] as EventInfo<T>).actions.Invoke(info);
    }

    //移除无参事件监听
    public void RemoveEventListener(string name, UnityAction action)
    {
        if (_eventDict.ContainsKey(name))
            (_eventDict[name] as EventInfo).actions -= action;
    }
    //移除有参事件监听
    public void RemoveEventListener<T>(string name, UnityAction<T> action)
    {
        if (_eventDict.ContainsKey(name))
            (_eventDict[name] as EventInfo<T>).actions -= action;
    }

    //清空事件列表
    public void ClearEvent()
    {
        _eventDict.Clear();
    }
}

// 事件响应空接口，用于支持可有可无的参数类型
public interface IEventInfo { }

// 无参数事件响应
public class EventInfo : IEventInfo
{
    public UnityAction actions;
    public EventInfo(UnityAction action)
    {
        actions += action;
    }
}

// 带参数事件响应
public class EventInfo<T> : IEventInfo
{
    public UnityAction<T> actions;
    public EventInfo(UnityAction<T> action)
    {
        actions += action;
    }
}