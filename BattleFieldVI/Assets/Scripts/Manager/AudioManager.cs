using DG.Tweening.Core.Easing;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

//全局管理音乐和音效的管理类，确保每个客户端只有一个
public class AudioManager : MonoBehaviour
{
    //音乐、2D音效、3D音效，音乐和2D音效的AudioSource放在GameManager下即可，但3D音效需要传入一个AudioClip和一个Vector3坐标
    public static GameObject gameManager;
    public static AudioSource BGMSource;
    //static AudioClip title, game1, game2, game3, win, lose;
    void Start()
    {
        gameManager = gameObject;
        BGMSource = gameObject.AddComponent<AudioSource>();
        //title = (AudioClip)Resources.Load("Audio/BGM/Title");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public static void PlayBGM(AudioClip audioClip)
    {
        AudioSource currentBGM = gameManager.AddComponent<AudioSource>();
        currentBGM.clip = audioClip;
        currentBGM.loop = true;
        currentBGM.volume = 0f;
        currentBGM.DOFade(GameSettings.MusicVolume, 1f);
        currentBGM.Play();
        BGMSource.DOFade(0f, 1f);
        Destroy(BGMSource, 1f);
        BGMSource = currentBGM;
    }

    public static void PauseBGM() => BGMSource.Pause();
    public static void ReplayBGM() => BGMSource.Play();
    public static void StopBGM() => PlayBGM(null);
    //public static void PlayTitleBGM() => PlayBGM(title);

    public static void PlaySFX2D(AudioClip sfxAudioClip)
    {
        AudioSource sfx = gameManager.AddComponent<AudioSource>();
        sfx.clip = sfxAudioClip;
        sfx.loop = false;
        sfx.volume = GameSettings.SFXVolume;
        //播放倍速
        sfx.pitch = 1.0f;
        //2D音效无空间混合
        sfx.spatialBlend = 0f;
        sfx.Play();
        //播放完毕后销毁
        Destroy(sfx, sfxAudioClip.length);
    }

    /// <summary>
    /// 生成音效游戏对象，执行完毕后销毁该对象，对象不会移动
    /// </summary>
    /// <param name="sfx3DAudioClip">要播放的音效Clip</param>
    /// <param name="Position">要生成音效的位置</param>
    public static void PlaySFX3D(AudioClip sfx3DAudioClip,Vector3 position)
    {
        // 创建一个新的空的GameObject
        GameObject soundObject = new GameObject("SFX3DAudioCarrier");
        soundObject.transform.position = position;
        // 添加一个AudioSource组件，并设置其属性
        AudioSource audioSource = soundObject.AddComponent<AudioSource>();
        audioSource.clip = sfx3DAudioClip;
        audioSource.spatialBlend = 1;  // 设置为3D音效
        audioSource.loop = false;
        audioSource.volume = GameSettings.SFXVolume;
        audioSource.Play();
        // 音效播放完成后，销毁GameObject
        Destroy(soundObject, sfx3DAudioClip.length);
    }
    /// <summary>
    /// 在指定Transform组件的GameObject上生成AudioSource，播放完后销毁
    /// </summary>
    /// <param name="sfx3DAudioClip">要播放的音效Clip</param>
    /// <param name="transform">要生成AudioSource的物体</param>
    public static void PlaySFX3D(AudioClip sfx3DAudioClip, Transform transform)
    {
        // 在给定的Transform的GameObject上添加一个AudioSource组件，并设置其属性
        AudioSource audioSource = transform.gameObject.AddComponent<AudioSource>();
        audioSource.clip = sfx3DAudioClip;
        audioSource.spatialBlend = 1;  // 设置为3D音效
        audioSource.loop = false;
        audioSource.volume = GameSettings.SFXVolume;
        audioSource.Play();
        // 音效播放完成后，销毁AudioSource组件
        Destroy(audioSource, sfx3DAudioClip.length);
    }

    /// <summary>
    /// 在指定Transform组件的GameObject上生成AudioSource，当音效未播放完就来了新的音效时将会覆盖音效
    /// </summary>
    /// <param name="sfx3DAudioClip"></param>
    /// <param name="transform"></param>
    public static void PlaySFX3DOverWrite(AudioClip sfx3DAudioClip, Transform transform)
    {
        // 在给定的Transform的GameObject上尝试获取AudioSource组件，没有则添加一个
        if(!transform.gameObject.TryGetComponent(out AudioSource audioSource))
            audioSource = transform.gameObject.AddComponent<AudioSource>();
        //暂停原本音效
        audioSource.Stop();
        //设置音效属性
        audioSource.clip = sfx3DAudioClip;
        audioSource.volume = GameSettings.SFXVolume;
        audioSource.spatialBlend = 1;  // 设置为3D音效
        audioSource.loop = false;
        audioSource.Play();
    }
}
