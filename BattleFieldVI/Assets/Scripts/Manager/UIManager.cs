using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager
{
    private static UIManager instance;
    public static UIManager Instance { get { if (instance == null) { instance = new UIManager(); } return instance; } }
    //UI面板路径字典
    private Dictionary<string, string> pathDict;
    //UI面板预制体缓存字典
    private Dictionary<string, GameObject> prefabDict;
    //已打开UI面板字典
    public Dictionary<string,BasePanel> panelDict;

    private Transform _uiRoot;
    public Transform UIRoot { get { if (_uiRoot == null) { _uiRoot = GameObject.Find("Canvas").transform; } return _uiRoot; } }

    private UIManager() 
    {
        InitPathDict();
    }

    private void InitPathDict()
    {
        prefabDict = new Dictionary<string, GameObject>();
        panelDict = new Dictionary<string, BasePanel>();

        pathDict = new Dictionary<string, string>()
        {
            {UIConst.SettingPanel,"Menu/SettingPanel" }
        };
    }

    public BasePanel OpenPanel(string name)
    {
        BasePanel panel = null;
        //检查当前是否已经打开
        if(panelDict.TryGetValue(name, out panel))
        {
            Debug.LogError("界面已经打开：" + name);
            return null;
        }

        //检查路径是否配置
        string path = "";
        if (!pathDict.TryGetValue(name, out path))
        {
            Debug.LogError("读取失败，界面名称错误或未配置路径: " + name);
            return null;
        }

        // 尝试使用缓存预制体
        GameObject panelPrefab = null;
        if (!prefabDict.TryGetValue(name, out panelPrefab))
        {
            string realPath = "Prefab/Panel/" + path;
            panelPrefab = Resources.Load<GameObject>(realPath) as GameObject;
            prefabDict.Add(name, panelPrefab);
        }

        // 打开UI界面
        GameObject panelObject = GameObject.Instantiate(panelPrefab, UIRoot, false);
        panel = panelObject.GetComponent<BasePanel>();
        panelDict.Add(name, panel);
        panel.OpenPanel(name);
        return panel;
    }

    public bool ClosePanel(string name)
    {
        BasePanel panel = null;
        if (!panelDict.TryGetValue(name, out panel))
        {
            Debug.LogError("关闭失败，界面未打开: " + name);
            return false;
        }

        panel.ClosePanel();
        return true;
    }

    //public void ShowTip(string tip)
    //{
    //    MenuTipPanel menuTipPanel = OpenPanel(UIConst.MenuTipPanel) as MenuTipPanel;
    //    menuTipPanel.InitTip(Globals.TipCreateOne);
    //}
}
