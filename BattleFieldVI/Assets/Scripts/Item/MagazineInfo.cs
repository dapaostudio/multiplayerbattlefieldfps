﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
[CreateAssetMenu(menuName = "FPS/MagazineInfo Data")]
//弹夹配置，用于记录弹夹预制体与弹夹对枪支数据影响
public class MagazineInfo : ScriptableObject
{
    //弹夹名称
    public string MagazineName;
    //弹夹预制体
    public GameObject MagazineGameObject;
    //弹夹相对位置-武器列表
    public List<MagazineOnFirearms> MagazineOnFirearms;

    //弹夹内子弹类型
    public BulletType BulletType;
    //弹夹内子弹预制体
    public GameObject BulletGameObject;
    //每分钟击发数
    public int RPM = 720;
    //弹夹最大容量
    public int AmmoInMag = 30;
    //总共的备弹数量
    public int AmmoCarried = 150;
    //换弹速度
    public float ReloadSpeed = 0.85f;

    //配件对枪械数据改变量
    public WeaponAttachDelta WeaponAttachDelta;
}

[System.Serializable]
public struct MagazineOnFirearms
{
    public string FirearmName;
    public Vector3 Position;
    public Quaternion Rotation;
}