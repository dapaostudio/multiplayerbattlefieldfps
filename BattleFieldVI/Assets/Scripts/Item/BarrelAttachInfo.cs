﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
[CreateAssetMenu(menuName = "FPS/BarrelAttachInfo Data")]
//枪管下挂配置，用于记录枪管下挂预制体与枪管下挂对枪支数据影响，Ability可以通过BarrelAttachGameObject来执行
public class BarrelAttachInfo : ScriptableObject
{
    //枪管下挂名称
    public string BarrelAttachName;
    //枪管下挂预制体
    public GameObject BarrelAttachGameObject;
    //相对位置-武器列表
    public List<BarrelAttachOnFirearms> BarrelAttachOnFirearms;

    //配件对枪械数据改变量
    public WeaponAttachDelta WeaponAttachDelta;
}

[System.Serializable]
public struct BarrelAttachOnFirearms
{
    public string FirearmName;
    public Vector3 Position;
    public Quaternion Rotation;
}