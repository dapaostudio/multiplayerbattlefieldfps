﻿using System.Collections;
using UnityEngine;

[System.Serializable]
public class WeaponAttachDelta
{
    //步兵基础伤害改变量
    public int DeltaSoldierDamage = 0;
    //轻甲基础伤害改变量
    public int DeltaLightArmorDamage = 0;
    //重甲基础伤害改变量
    public int DeltaHeavyArmorDamage = 0;
    //后坐力持续时间改变量
    public float DeltaRecoliFadeTime = 0f;
    //瞄准后坐力比率改变量
    public float DeltaRecoilAimFraction = 0f;
    //子弹初速度改变量
    public float DeltaBulletSpeed = 0f;
    //武器腰射散射角度改变量
    public float DeltaSpreadAngle = 0f;
    //每秒射速改变量
    public float DeltaFireRate = 0f;
    //水平后坐力改变量
    public float DeltaHorizontalRecoil = 0f;
    //垂直后坐力改变量
    public float DeltaVerticalRecoilRange = 0f;
    //震屏幅度绝对值改变量
    public float DeltaScreenSpringRange = 0f;
    //震屏回正速度改变量
    public float DeltaDampSpeed = 0f;
}
