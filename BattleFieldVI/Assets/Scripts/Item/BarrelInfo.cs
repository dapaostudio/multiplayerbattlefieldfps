﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
[CreateAssetMenu(menuName = "FPS/BarrelInfo Data")]
//枪口配置，用于记录枪口预制体与枪口对枪支数据影响
public class BarrelInfo : ScriptableObject
{
    //枪口名称
    public string BarrelName;
    //枪口预制体
    public GameObject BarrelGameObject;
    //枪口类型
    public BarrelType BarrelType;
    //枪口位置偏移
    public Vector3 DeltaMuzzlePointOffset;
    //相对位置-武器列表
    public List<BarrelOnFirearms> BarrelOnFirearms;

    //配件对枪械数据改变量
    public WeaponAttachDelta WeaponAttachDelta;
}

[System.Serializable]
public struct BarrelOnFirearms
{
    public string FirearmName;
    public Vector3 Position;
    public Quaternion Rotation;
}

//枪管类型 用于匹配开火音效，分为正常枪管、消音器、长枪管、短枪管
public enum BarrelType
{
    Normal,
    Muffling,
    Long,
    Short
}