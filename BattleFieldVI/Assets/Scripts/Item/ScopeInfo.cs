using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
[CreateAssetMenu(menuName = "FPS/ScopeInfo Data")]
//瞄具配置，用于记录瞄具预制体
public class ScopeInfo : ScriptableObject
{
    //瞄具名称
    public string ScopeName;
    //瞄具预制体
    public GameObject ScopeGameObject;
    //瞄具瞄准时FOV
    public float EyeFov;
    public float GunFov;
    //瞄具相机坐标
    public Vector3 GunCameraPostion;
    //瞄具相对位置-武器表
    public List<ScopeOnFirearms> ScopesOnFirearms;
}

[System.Serializable]
public struct ScopeOnFirearms
{
    public string FirearmName;
    public Vector3 Position;
    public Quaternion Rotation;
}
