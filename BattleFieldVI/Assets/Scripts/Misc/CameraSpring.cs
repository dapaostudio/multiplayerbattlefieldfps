using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//控制摄像机震动的脚本，一般只作用在步兵，但也可通过修改SpringTransform的方式来给固定武器或者载具火器施加震动
public class CameraSpring : MonoBehaviour
{
    //接受震屏的Transform，一般为GunCamera的上一级，由上层代码指定
    public Transform CameraSpringTransform;
    //震屏幅度绝对值
    public float ScreenSpringRange = 0f;
    //震屏数值更新频率
    public float Frequence = 25f;
    //震屏回正速度
    public float DampSpeed = 15f;
    //震屏实时值
    public Vector3 SpringValues = new();

    //每帧震屏变化值
    Vector3 dampValues = new();

    
    void Update()
    {
        //如果未指定震屏对象，就不执行震屏
        if (CameraSpringTransform == null) return;
        UpdateSpring();
    }

    //更新震屏实时值
    void UpdateSpring()
    {
        //每帧更新震屏值
        SpringValues -= Time.deltaTime * Frequence * dampValues;
        //更新下一帧需要的震屏值，最终可回正
        dampValues = Vector3.Lerp(dampValues, SpringValues - Vector3.zero, DampSpeed * Time.deltaTime);
        //更新震屏对象Rotation值进行震屏
        CameraSpringTransform.localRotation = Quaternion.Slerp(CameraSpringTransform.localRotation, Quaternion.Euler(SpringValues), Time.deltaTime * 10);
    }

    //切换武器或配件时，更新震屏范围与震屏频率
    public void SetSpringRangeAndFrequence(float springRange, float frequence)
    {
        ScreenSpringRange = springRange;
        Frequence = frequence;
    }
    //切换模式时，更新震屏Transform
    public void SetSpringTransform(Transform transform)
    {
        CameraSpringTransform = transform;
    }

    //开启震屏，震屏通过震屏幅度随机选择范围
    public void StartCameraSpring()
    {
        SpringValues = new Vector3(0, 0, Random.Range(-ScreenSpringRange, ScreenSpringRange));
    }

    //开启震屏，震屏幅度为指定数值
    public void StartCameraSpring(float screenSpringValue)
    {
        SpringValues = new Vector3(0, 0, Random.Range(0, 100) % 2 == 0 ? -screenSpringValue : screenSpringValue);
    }
}
