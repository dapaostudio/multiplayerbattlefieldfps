using Assets.Scripts.Weapon;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "FPS/Firearm Data")]
public class FirearmData : ScriptableObject
{
    //枪械名称
    public string FirearmName;
    //开火粒子特效
    public ParticleSystem MuzzleParticle;
    //枪械音效配置文件，包含开火、装弹、弹壳音效
    public FirearmsAudioData FirearmsAudioData;
    //武器类型
    public WeaponType WeaponType;
    //武器槽位，0为主武器，1为副武器，最大到4
    public WeaponSlot WeaponSlot;
    //支持的开火模式
    public List<FireMode> SupportFireModes;
    //武器数值信息WeaponInfo，存子弹类型-基础伤害的DamageInfo表
    public List<WeaponDamageInfo> WeaponDamageInfoTable;
    //支持的弹夹信息，弹夹里只存子弹类型
    public List<MagazineInfo> SupportMagazineInfos;
    //默认弹夹
    public MagazineInfo DefaultMagazine;
    //支持的枪管信息
    public List<BarrelInfo> SupportBarrelInfos;
    //默认枪管
    public BarrelInfo DefaultBarrel;
    //支持的枪管下挂信息
    public List<BarrelAttachInfo> SupportBarrelAttachInfos;
    //默认枪管下挂
    public BarrelAttachInfo DefaultBarrelAttach;
    //支持的瞄具信息，包含照门
    public List<ScopeInfo> SupportScopeInfos;
    //默认瞄具
    public ScopeInfo DefaultScope;
    //过热参数
    public HeatInfo HeatInfo;
    //归零点距离
    public List<float> ZeroPointInfo;
    //默认枪械开火属性
    public RuntimeInfoForFire WeaponBaseFireConfig;
    //枪械连发时的精准度与后坐力曲线
    public AnimationCurve ControlCurve;
    //枪械到达最不可控所需连发数
    public int MaxControlShotCount;
}

public enum WeaponSlot
{
    Main,
    Secondary,
    Third,
    Fourth,
    Fifth
}
