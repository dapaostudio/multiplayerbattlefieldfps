﻿using System.Collections;
using UnityEngine;

//武器的伤害表，每个武器配置里维护一个List，同时再运行时维护一个CurrentDamageInfo，通过与弹夹的Bullet的BulletType对比得出当前装弹伤害所需的伤害和伤害曲线
[System.Serializable]
public class WeaponDamageInfo 
{
    //子弹类型
    public BulletType BulletType;
    //子弹伤害数值
    public WeaponDamageValue DamageValue;
}
[System.Serializable]
public class WeaponDamageValue
{
    //步兵基础伤害
    public int SoldierDamage;
    //轻甲基础伤害
    public int LightArmorDamage;
    //重甲基础伤害
    public int HeavyArmorDamage;
    //步兵最高溅射伤害
    public int BoomSoldierDamage = 0;
    //轻甲最高溅射伤害
    public int BoomLightArmorDamage = 0;
    //重甲最高溅射伤害
    public int BoomHeavyArmorDamage = 0;
    //爆炸范围
    public float BoomRange = 0.1f;
    //对步兵伤害曲线
    public AnimationCurve BoomSoldierDamageCurve = new();
    //对装甲伤害曲线
    public AnimationCurve BoomArmorDamageCurve = new();
}

//普通、重型、轻型、碰炸穿甲、碰炸高爆、近炸、定时
public enum BulletType
{
    Normal,
    Heavy,
    Light,
    AP,
    HE,
    Proximity,
    Timed
};