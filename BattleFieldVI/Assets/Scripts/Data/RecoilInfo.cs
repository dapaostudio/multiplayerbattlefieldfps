﻿using System.Collections;
using UnityEngine;

//后坐力属性，包含水平范围、垂直范围、屏幕震动回正速度、
[System.Serializable]
public class RecoilStatus
{
    //动画幅度-时间曲线，要在Inspector面板调整
    public AnimationCurve RecoilCurve;
    //水平后坐力
    public float HorizontalRecoil;
    //垂直后坐力
    public float VerticalRecoilRange;

    public void AddEqump()
    {

    }

    public void RemoveEqump()
    {

    }
}
