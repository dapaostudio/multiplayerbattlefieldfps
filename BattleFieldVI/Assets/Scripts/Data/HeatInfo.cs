﻿
//武器固有的过热参数，一般拥有过热参数的武器均不需要运行时修改本参数
[System.Serializable]
public struct HeatInfo
{
    //最大过热热量，为0代表不会过热
    public int MaxHeat;
    //单发增加热度
    public int HeatPerShot;
    //每秒散热
    public int HeatReducePerSec;
    //过热时每秒散热
    public int OverHeatReducePerSec;
}
