using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "FPS/FootStep Audio Data")]
public class FootStepAudioData : ScriptableObject
{
    public List<FootStepAudio> FootStepAudios = new List<FootStepAudio>();
}

[System.Serializable]
public class FootStepAudio
{
    //脚步材质Tag
    public string Tag;
    //该材质的全部踏步音效合集
    public List<AudioClip> AudioClips = new List<AudioClip>();
}