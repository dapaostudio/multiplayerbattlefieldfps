using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "FPS/Impact Audio Data")]
public class ImpactAudioData : ScriptableObject
{
    public List<ImpactTagsWithAudio> ImpactTagsWithAudios;
}

[System.Serializable]
public class ImpactTagsWithAudio
{
    //�ӵ�������ײ���Tag
    public string Tag;
    //�ӵ�ײ����Ч
    public List<AudioClip> ImpactAudioClips;
}
