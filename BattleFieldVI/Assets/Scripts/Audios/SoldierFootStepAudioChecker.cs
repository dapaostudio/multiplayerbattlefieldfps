﻿using System.Collections;
using UnityEngine;


public class SoldierFootStepAudioChecker : MonoBehaviour
{
    //射线Layer遮罩
    public LayerMask LayerMask;
    //脚步声配置文件
    FootStepAudioData FootStepAudioData;
    Soldier soldier;
    //用于计算脚步声CD
    float nextPlayTime;
    CharacterController characterController;
    //每一步的距离，未来要从SoldierData里获取，TODO
    float oneWalkStepLenth;
    float oneSprintStepLenth;
    void Start()
    {
        soldier = GetComponent<Soldier>();
        characterController = GetComponent<CharacterController>();
        oneWalkStepLenth = 1.8f;
        oneSprintStepLenth = 3f;
        FootStepAudioData = (FootStepAudioData)Resources.Load("Configs/FootSteps/FootStepAudioData");
    }


    void Update()
    {
        //计算是否可以播放下一次脚步声，放在这儿是为了能第一时间播放脚步声
        nextPlayTime += Time.deltaTime;
        //角色非活动状态或者角色不在地面上时不触发脚步声
        if (!characterController.isGrounded || soldier.Status == SoldierStatus.CantMove) return;
        //角色移动过慢时不触发脚步声
        if (soldier.SoldierSpeed < 0.1f) return;
        CheckGroundTag();
    }

    void CheckGroundTag()
    {
        //计算射线终点坐标
        Vector3 rayEndPos = CalculateFootRayEndPos();
        //判断是否检测到
        bool isHit = Physics.Linecast(transform.position, rayEndPos, out RaycastHit hitInfo, LayerMask);
        //绘制检测线
        //Debug.DrawLine(transform.position, rayEndPos, Color.red, 0.25f);
        //如果没检测到地面则直接返回
        if (!isHit) return;
        //遍历检测到的Tag与脚步声Tag
        foreach (FootStepAudio item in FootStepAudioData.FootStepAudios)
        {
            //如果检测到的Tag与当前遍历到的脚部声音Tag不匹配则继续尝试下一个
            if (!hitInfo.collider.CompareTag(item.Tag)) continue;
            //如果匹配到则播放脚步音效
            PlayFootStepSound(item);
            break;
        }
    }

    Vector3 CalculateFootRayEndPos()
    {
        //返回角色足部位置为射线检测终点
        return soldier.SoldierFoot.transform.position; ;
    }

    //计算播放音效CD
    bool CheckPlayStepSoundColdingDown()
    {
        if (soldier.Status == SoldierStatus.Sprint)
        {
            //跑步状态步频
            return nextPlayTime < oneSprintStepLenth / soldier.SoldierSpeed;
        }
        else if (soldier.Status != SoldierStatus.CantMove)
        {
            //非跑步且可以移动状态步频，未来可以再细化
            return nextPlayTime < oneWalkStepLenth / soldier.SoldierSpeed;
        }
        else
        {   //其他状态，返回冷却中
            return true;
        }
    }

    void PlayFootStepSound(FootStepAudio item)
    {
        //播放频率跟玩家速度相关
        if (CheckPlayStepSoundColdingDown()) return;
        //随机播放一个音效
        int audioIndex = Random.Range(0, item.AudioClips.Count);
        AudioClip footStepAudioClip = item.AudioClips[audioIndex];
        AudioManager.PlaySFX3D(footStepAudioClip, soldier.SoldierFoot);
        nextPlayTime = 0f;
    }
}
