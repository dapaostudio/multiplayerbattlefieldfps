using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "FPS/Weapon Audio Data")]
public class FirearmsAudioData : ScriptableObject
{
    //正常射击音效，用List增加扩展性
    public List<AudioClip> ShootingAudios;
    //消音器设计音效，用List增加扩展性
    public List<AudioClip> LowAmmoShootingAudio;
    //弹壳音效
    public List<AudioClip> CasigAudios;
    //机匣震动音效（可以混合一个机匣震动音效，音量与剩余弹量有关）
    public AudioClip LowAmmoMixAudio;
    //爆炸子弹爆炸音效
    public AudioClip BulletExplosionAudio;
    //子弹破空声音效（可以扩展远近时的破空声？）
    public AudioClip BulletAirPoppinAudio;
    //装弹音效，枪机内有子弹
    public AudioClip ReloadLeftAudio;
    //装弹音效，枪机内没有子弹
    public AudioClip ReloadOutOfAudio;

}
