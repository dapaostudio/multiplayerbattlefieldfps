using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GameSettings
{
    public static float MasterVolume = 1.0f;
    public static float MusicVolume = 1.0f;
    public static float SFXVolume = 1.0f;
    //视角控制灵敏度，可通过字典扩展更多倍镜时的灵敏度，此处先实现基础的
    public static float ViewSensitive = 5f;
}
