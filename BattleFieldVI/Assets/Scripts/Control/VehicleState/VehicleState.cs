using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEditor.Rendering.LookDev;
using UnityEngine;
using UnityEngine.InputSystem;

public class VehicleState : IPlayerState
{
    protected Player player;
    public Player Player { get => player; set => player = value; }

    public void EnterState()
    {
        GetCAMS();
        player.SetCAM();

        player.PlayerInput.Vehicles.Enable();

        player.PlayerInput.Vehicles.Turn.performed += OnHorizontal;
        player.PlayerInput.Vehicles.Turn.canceled += OnHorizontal;

        player.PlayerInput.Vehicles.Speed.performed += OnVertical;
        player.PlayerInput.Vehicles.Speed.canceled += OnVertical;

        player.PlayerInput.Vehicles.ViewControl.performed += OnViewControl;
        player.PlayerInput.Vehicles.ViewControl.canceled += OnViewControl;

        player.PlayerInput.Vehicles.LeaveVehicle.started += OnExitVehicle;
        player.PlayerInput.Vehicles.LeaveVehicle.performed += OnExitVehicle;
        player.PlayerInput.Vehicles.LeaveVehicle.canceled += OnExitVehicle;
    }

    public void ExitState()
    {
        player.PlayerInput.Vehicles.Turn.performed -= OnHorizontal;
        player.PlayerInput.Vehicles.Turn.canceled -= OnHorizontal;

        player.PlayerInput.Vehicles.Speed.performed -= OnVertical;
        player.PlayerInput.Vehicles.Speed.canceled -= OnVertical;

        player.PlayerInput.Vehicles.ViewControl.performed -= OnViewControl;
        player.PlayerInput.Vehicles.ViewControl.canceled -= OnViewControl;

        player.PlayerInput.Vehicles.LeaveVehicle.started += OnExitVehicle;
        player.PlayerInput.Vehicles.LeaveVehicle.performed += OnExitVehicle;
        player.PlayerInput.Vehicles.LeaveVehicle.canceled += OnExitVehicle;

        player.PlayerInput.Vehicles.Disable();

        player.CloseCAM();
    }

    public void GetCAMS()
    {
        player.Avatar3rdViewCamera = player.CurrentAvatar.GetComponent<Vehicle>().Vehicle3rdViewCamera;
    }

    public virtual void OnHorizontal(InputAction.CallbackContext context)
    {
        Player.CurrentAvatar.GetComponent<Vehicle>().VehicleControl.GetHorizontalInput(context.ReadValue<float>());
    }

    public virtual void OnVertical(InputAction.CallbackContext context)
    {
        Player.CurrentAvatar.GetComponent<Vehicle>().VehicleControl.GetVerticalInput(context.ReadValue<float>());
    }

    public virtual void OnViewControl(InputAction.CallbackContext context)
    {
        Player.CurrentAvatar.GetComponent<Vehicle>().VehicleControl.GetViewInput(context.ReadValue<Vector2>());
    }

    public virtual void OnExitVehicle(InputAction.CallbackContext context) 
    {
        if (context.performed)
        {
            player.InteractionEvent.Invoke(player.CurrentAvatar);
            player.InteractionEvent -= player.ResumeSoldier;
        }
    }
}
