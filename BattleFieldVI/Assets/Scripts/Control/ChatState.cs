using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChatState : IPlayerState
{
    protected Player player;
    public Player Player { get => player; set => player = value; }

    public void EnterState()
    {
        
    }

    public void ExitState()
    {
        
    }

    public void GetCAMS()
    {
        
    }
}

