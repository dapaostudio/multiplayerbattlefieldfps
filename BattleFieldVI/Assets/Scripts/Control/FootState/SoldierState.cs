using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using static RootMotion.FinalIK.InteractionObject;

//IplayerState峪喘噐俊辺補秘旺距喘��醤悶議糞�嶬擽墫Φ鎚祇�和��宸戦峪俶勣距喘斤哘圭隈賜委並周勧弓竃肇軸辛
public class SoldierState : IPlayerState
{
    protected Player player;
    public Player Player { get => player; set => player = value; }

    //序秘化佩彜蓑扮鰯協荷恬
    public void EnterState()
    {
        player.PlayerInput.Soldier.Enable();
        /*！！！！！！！！！！！！！！！！！！！！叔弼陣崙！！！！！！！！！！！！！！！！！！！！*/
        player.PlayerInput.Soldier.Move.performed += OnMove;
        player.PlayerInput.Soldier.Move.canceled += OnMove;

        player.PlayerInput.Soldier.ViewControl.performed += OnViewControl;
        player.PlayerInput.Soldier.ViewControl.canceled += OnViewControl;

        player.PlayerInput.Soldier.Sprint.performed += OnSprint;
        player.PlayerInput.Soldier.Sprint.canceled += OnSprint;

        player.PlayerInput.Soldier.Crouch.performed += OnCrouch;
        player.PlayerInput.Soldier.Crouch.canceled += OnCrouch;

        player.PlayerInput.Soldier.SwitchCrouch.performed += OnSwitchCrouch;
        player.PlayerInput.Soldier.SwitchLie.performed += OnSwitchLie;
        player.PlayerInput.Soldier.Jump.performed += OnJump;
        player.PlayerInput.Soldier.Climb.performed += OnClimb;
        player.PlayerInput.Soldier.Slide.performed += OnSlide;
        player.PlayerInput.Soldier.OpenParachute.performed += OnOpenParachute;
        player.PlayerInput.Soldier.CloseParachute.performed += OnCloseParachute;
        /*！！！！！！！！！！！！！！！！！！！！冷匂陣崙！！！！！！！！！！！！！！！！！！！！*/
        player.PlayerInput.Soldier.Trigger.performed += OnTrigger;
        player.PlayerInput.Soldier.Trigger.canceled += OnTrigger;

        player.PlayerInput.Soldier.SubTrigger.performed += OnSubTrigger;

        player.PlayerInput.Soldier.Aim.performed += OnAim;
        player.PlayerInput.Soldier.Aim.canceled += OnAim;

        player.PlayerInput.Soldier.Charge.started += OnCharge;
        player.PlayerInput.Soldier.Charge.performed += OnCharge;

        player.PlayerInput.Soldier.Melee.performed += OnMelee;

        player.PlayerInput.Soldier.MeleeSprint.started += OnMeleeSprint;
        player.PlayerInput.Soldier.MeleeSprint.performed += OnMeleeSprint;

        player.PlayerInput.Soldier.Reload.performed += OnReload;
        player.PlayerInput.Soldier.UseWeaponAbility.performed += OnUseWeaponAbility;
        player.PlayerInput.Soldier.SwitchScope.performed += OnSwitchScope;

        player.PlayerInput.Soldier.OpenWeaponConfigPanel.performed += OnOpenWeaponConfigPanel;
        player.PlayerInput.Soldier.OpenWeaponConfigPanel.canceled += OnOpenWeaponConfigPanel;

        player.PlayerInput.Soldier.SwitchFireMode.performed += OnSwitchFireMode;
        player.PlayerInput.Soldier.SwitchZeroPoint.performed += OnSwitchZeroPoint;

        player.PlayerInput.Soldier.ThrowableWeapon.performed += OnThrowableWeapon;
        player.PlayerInput.Soldier.ThrowableWeapon.canceled += OnThrowableWeapon;

        player.PlayerInput.Soldier.SwitchWeapon.performed += OnSwitchWeapon;
        player.PlayerInput.Soldier.SwitchToWeapon1.performed += OnSwitchToWeapon1;
        player.PlayerInput.Soldier.SwitchToWeapon2.performed += OnSwitchToWeapon2;
        player.PlayerInput.Soldier.SwitchToWeapon3.performed += OnSwitchToWeapon3;
        player.PlayerInput.Soldier.SwitchToWeapon4.performed += OnSwitchToWeapon4;
        player.PlayerInput.Soldier.SwitchToWeapon5.performed += OnSwitchToWeapon5;
        /*！！！！！！！！！！！！！！！！！！！！順中住札！！！！！！！！！！！！！！！！！！！！*/
        player.PlayerInput.Soldier.Mark.performed += OnMark;

        player.PlayerInput.Soldier.InterAct.started += OnInterAct;
        player.PlayerInput.Soldier.InterAct.performed += OnInterAct;
        player.PlayerInput.Soldier.InterAct.canceled += OnInterAct;

        player.PlayerInput.Soldier.CallHelp.started += OnCallHelp;
        player.PlayerInput.Soldier.CallHelp.performed += OnCallHelp;

        player.PlayerInput.Soldier.PickUp.started += OnPickUp;
        player.PlayerInput.Soldier.PickUp.performed += OnPickUp;

        player.PlayerInput.Soldier.SignalWheel.performed += OnSignalWheel;
        player.PlayerInput.Soldier.SignalWheel.canceled += OnSignalWheel;

        player.PlayerInput.Soldier.OpenChatLobby.performed += OnOpenChatLobby;
        player.PlayerInput.Soldier.OpenChatTeam.performed += OnOpenChatTeam;
        player.PlayerInput.Soldier.OpenChatAll.performed += OnOpenChatAll;
        player.PlayerInput.Soldier.Map.performed += OnMap;
        player.PlayerInput.Soldier.SwitchMapScale.performed += OnSwitchMapScale;

        player.PlayerInput.Soldier.OpenScoreBoard.performed += OnOpenScoreBoard;
        player.PlayerInput.Soldier.OpenScoreBoard.canceled += OnOpenScoreBoard;
    }

    //宣蝕化佩彜蓑扮廣��荷恬
    public void ExitState()
    {
        /*！！！！！！！！！！！！！！！！！！！！叔弼陣崙！！！！！！！！！！！！！！！！！！！！*/
        player.PlayerInput.Soldier.Move.performed -= OnMove;
        player.PlayerInput.Soldier.Move.canceled -= OnMove;

        player.PlayerInput.Soldier.ViewControl.performed -= OnViewControl;
        player.PlayerInput.Soldier.ViewControl.canceled -= OnViewControl;

        player.PlayerInput.Soldier.Sprint.performed -= OnSprint;
        player.PlayerInput.Soldier.Sprint.canceled -= OnSprint;

        player.PlayerInput.Soldier.Crouch.performed -= OnCrouch;
        player.PlayerInput.Soldier.Crouch.canceled -= OnCrouch;

        player.PlayerInput.Soldier.SwitchCrouch.performed -= OnSwitchCrouch;
        player.PlayerInput.Soldier.SwitchLie.performed -= OnSwitchLie;
        player.PlayerInput.Soldier.Jump.performed -= OnJump;
        player.PlayerInput.Soldier.Climb.performed -= OnClimb;
        player.PlayerInput.Soldier.Slide.performed -= OnSlide;
        player.PlayerInput.Soldier.OpenParachute.performed -= OnOpenParachute;
        player.PlayerInput.Soldier.CloseParachute.performed -= OnCloseParachute;
        /*！！！！！！！！！！！！！！！！！！！！冷匂陣崙！！！！！！！！！！！！！！！！！！！！*/
        player.PlayerInput.Soldier.Trigger.performed -= OnTrigger;
        player.PlayerInput.Soldier.Trigger.canceled -= OnTrigger;

        player.PlayerInput.Soldier.SubTrigger.performed -= OnSubTrigger;

        player.PlayerInput.Soldier.Aim.performed -= OnAim;
        player.PlayerInput.Soldier.Aim.canceled -= OnAim;

        player.PlayerInput.Soldier.Charge.started -= OnCharge;
        player.PlayerInput.Soldier.Charge.performed -= OnCharge;

        player.PlayerInput.Soldier.Melee.performed -= OnMelee;

        player.PlayerInput.Soldier.MeleeSprint.started -= OnMeleeSprint;
        player.PlayerInput.Soldier.MeleeSprint.performed -= OnMeleeSprint;

        player.PlayerInput.Soldier.Reload.performed -= OnReload;
        player.PlayerInput.Soldier.UseWeaponAbility.performed -= OnUseWeaponAbility;
        player.PlayerInput.Soldier.SwitchScope.performed -= OnSwitchScope;

        player.PlayerInput.Soldier.OpenWeaponConfigPanel.performed -= OnOpenWeaponConfigPanel;
        player.PlayerInput.Soldier.OpenWeaponConfigPanel.canceled -= OnOpenWeaponConfigPanel;

        player.PlayerInput.Soldier.SwitchFireMode.performed -= OnSwitchFireMode;
        player.PlayerInput.Soldier.SwitchZeroPoint.performed -= OnSwitchZeroPoint;

        player.PlayerInput.Soldier.ThrowableWeapon.performed -= OnThrowableWeapon;
        player.PlayerInput.Soldier.ThrowableWeapon.canceled -= OnThrowableWeapon;

        player.PlayerInput.Soldier.SwitchWeapon.performed -= OnSwitchWeapon;
        player.PlayerInput.Soldier.SwitchToWeapon1.performed -= OnSwitchToWeapon1;
        player.PlayerInput.Soldier.SwitchToWeapon2.performed -= OnSwitchToWeapon2;
        player.PlayerInput.Soldier.SwitchToWeapon3.performed -= OnSwitchToWeapon3;
        player.PlayerInput.Soldier.SwitchToWeapon4.performed -= OnSwitchToWeapon4;
        player.PlayerInput.Soldier.SwitchToWeapon5.performed -= OnSwitchToWeapon5;
        /*！！！！！！！！！！！！！！！！！！！！順中住札！！！！！！！！！！！！！！！！！！！！*/
        player.PlayerInput.Soldier.Mark.performed -= OnMark;

        player.PlayerInput.Soldier.InterAct.started -= OnInterAct;
        player.PlayerInput.Soldier.InterAct.performed -= OnInterAct;
        player.PlayerInput.Soldier.InterAct.canceled -= OnInterAct;

        player.PlayerInput.Soldier.CallHelp.started -= OnCallHelp;
        player.PlayerInput.Soldier.CallHelp.performed -= OnCallHelp;

        player.PlayerInput.Soldier.PickUp.started -= OnPickUp;
        player.PlayerInput.Soldier.PickUp.performed -= OnPickUp;

        player.PlayerInput.Soldier.SignalWheel.performed -= OnSignalWheel;
        player.PlayerInput.Soldier.SignalWheel.canceled -= OnSignalWheel;

        player.PlayerInput.Soldier.OpenChatLobby.performed -= OnOpenChatLobby;
        player.PlayerInput.Soldier.OpenChatTeam.performed -= OnOpenChatTeam;
        player.PlayerInput.Soldier.OpenChatAll.performed -= OnOpenChatAll;
        player.PlayerInput.Soldier.Map.performed -= OnMap;
        player.PlayerInput.Soldier.SwitchMapScale.performed -= OnSwitchMapScale;

        player.PlayerInput.Soldier.OpenScoreBoard.performed -= OnOpenScoreBoard;
        player.PlayerInput.Soldier.OpenScoreBoard.canceled -= OnOpenScoreBoard;

        player.PlayerInput.Soldier.Disable();
    }

    public void GetCAMS()
    {

    }

    /*！！！！！！！！！！！！！！！！！！！！叔弼陣崙！！！！！！！！！！！！！！！！！！！！*/
    //聞喘WSAD陣崙叔弼卞強
    public virtual void OnMove(InputAction.CallbackContext context)
    {
        Player.Soldier.GetComponent<Soldier>().SoldierControl.GetMoveInput(context.ReadValue<Vector2>());
    }

    //聞喘報炎陣崙叔弼篇叔
    public virtual void OnViewControl(InputAction.CallbackContext context)
    {
        Player.Soldier.GetComponent<Soldier>().SoldierControl.GetViewInput(context.ReadValue<Vector2>());
    }

    public virtual void OnSprint(InputAction.CallbackContext context)
    {
        if (context.performed)
        {
            Player.Soldier.GetComponent<Soldier>().SoldierControl.Sprint(true);
        }
        else if (context.canceled)
        {
            Player.Soldier.GetComponent<Soldier>().SoldierControl.Sprint(false);
        }
    }

    public virtual void OnCrouch(InputAction.CallbackContext context) 
    {
        if (context.performed)
        {
            Player.Soldier.GetComponent<Soldier>().SoldierControl.Crouch(true);
        }
        else if (context.canceled)
        {
            Player.Soldier.GetComponent<Soldier>().SoldierControl.Crouch(false);
        }
    }

    public virtual void OnSwitchCrouch(InputAction.CallbackContext context)
    {
        Player.Soldier.GetComponent<Soldier>().SoldierControl.SwitchCrouch();
    }

    public virtual void OnSwitchLie(InputAction.CallbackContext context)
    {

    }

    public virtual void OnJump(InputAction.CallbackContext context)
    {
        Player.Soldier.GetComponent<Soldier>().SoldierControl.Jump();
    }

    public virtual void OnClimb(InputAction.CallbackContext context)
    {

    }

    public virtual void OnSlide(InputAction.CallbackContext context)
    {

    }

    public virtual void OnOpenParachute(InputAction.CallbackContext context)
    {

    }

    public virtual void OnCloseParachute(InputAction.CallbackContext context)
    {

    }

    /*！！！！！！！！！！！！！！！！！！！！冷匂陣崙！！！！！！！！！！！！！！！！！！！！*/
    public virtual void OnTrigger(InputAction.CallbackContext context)
    {
        if (context.performed)
        {
            Player.WeaponManager.CarriedWeapon.HoldMainTrigger();
        }
        else if (context.canceled)
        {
            Player.WeaponManager.CarriedWeapon.ReleaseMainTrigger();
        }
    }

    public virtual void OnSubTrigger(InputAction.CallbackContext context)
    {

    }

    public virtual void OnAim(InputAction.CallbackContext context)
    {
        if (context.performed)
        {
            Player.WeaponManager.CarriedWeapon.Aim();
        }
        else if (context.canceled)
        {
            Player.WeaponManager.CarriedWeapon.ReleaseSubTrigger();
        }
    }

    public virtual void OnCharge(InputAction.CallbackContext context)
    {

    }

    public virtual void OnReload(InputAction.CallbackContext context)
    {
        Player.WeaponManager.CarriedWeapon.ReloadAmmo();
    }

    public virtual void OnMelee(InputAction.CallbackContext context)
    {

    }

    public virtual void OnMeleeSprint(InputAction.CallbackContext context)
    {

    }

    public virtual void OnUseWeaponAbility(InputAction.CallbackContext context)
    {

    }

    public virtual void OnSwitchScope(InputAction.CallbackContext context)
    {

    }

    public virtual void OnOpenWeaponConfigPanel(InputAction.CallbackContext context)
    {

    }

    public virtual void OnSwitchFireMode(InputAction.CallbackContext context)
    {

    }

    public virtual void OnSwitchZeroPoint(InputAction.CallbackContext context)
    {

    }

    public virtual void OnThrowableWeapon(InputAction.CallbackContext context)
    {

    }

    public virtual void OnSwitchWeapon(InputAction.CallbackContext context)
    {

    }

    public virtual void OnSwitchToWeapon1(InputAction.CallbackContext context)
    {

    }

    public virtual void OnSwitchToWeapon2(InputAction.CallbackContext context)
    {

    }

    public virtual void OnSwitchToWeapon3(InputAction.CallbackContext context)
    {

    }

    public virtual void OnSwitchToWeapon4(InputAction.CallbackContext context)
    {

    }

    public virtual void OnSwitchToWeapon5(InputAction.CallbackContext context)
    {

    }

    /*！！！！！！！！！！！！！！！！！！！！住札順中！！！！！！！！！！！！！！！！！！！！*/

    public virtual void OnMark(InputAction.CallbackContext context)
    {

    }

    public virtual void OnInterAct(InputAction.CallbackContext context)
    {
        if (context.started)
        {

        }
        else if (context.performed)
        {
            player.InteractionEvent.Invoke(player.TempVehicle);
            player.InteractionEvent -= player.EnterVehicle;
            player.InteractionEvent += player.ResumeSoldier;
        }
        else if (context.canceled)
        {

        }
    }

    public virtual void OnCallHelp(InputAction.CallbackContext context)
    {

    }

    public virtual void OnPickUp(InputAction.CallbackContext context)
    {

    }

    public virtual void OnSignalWheel(InputAction.CallbackContext context)
    {

    }

    public virtual void OnOpenChatLobby(InputAction.CallbackContext context)
    {

    }

    public virtual void OnOpenChatTeam(InputAction.CallbackContext context)
    {

    }

    public virtual void OnOpenChatAll(InputAction.CallbackContext context)
    {

    }

    public virtual void OnOpenMenu(InputAction.CallbackContext context)
    {

    }

    public virtual void OnMap(InputAction.CallbackContext context)
    {

    }

    public virtual void OnSwitchMapScale(InputAction.CallbackContext context)
    {

    }

    public virtual void OnOpenScoreBoard(InputAction.CallbackContext context)
    {

    }
}
