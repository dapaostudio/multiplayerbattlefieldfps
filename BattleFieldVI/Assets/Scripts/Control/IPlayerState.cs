using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPlayerState
{
    //保持对Player的引用
    public Player Player { get; set; }
    //用于更新摄像机
    public void GetCAMS();
    //用于在进入状态时订阅注册委托函数
    public void EnterState();
    //用于在离开状态时注销委托函数
    public void ExitState();
}
 