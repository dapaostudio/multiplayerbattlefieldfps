﻿using System.Collections;
using UnityEngine;

namespace Assets.Scripts.Weapon
{
    //运行时的武器开火用参数，用于开火时检查调用，每次增减切换配件时都会重新计算，由弹夹参数作为基础参数替换后叠加其他配件而来
    [System.Serializable]
    public class RuntimeInfoForFire
    {
        //当前武器子弹类型
        public BulletType CurrentBulletType;
        //当前武器伤害配置
        public WeaponDamageValue CurrentWeaponDamageConfig;
        //后坐力持续时间
        public float RecoilFadeTime = 0.3f;
        //瞄准后坐力比例
        public float RecoilAimFraction = 0.5f;
        //子弹初速度
        public float BulletSpeed;
        //武器腰射散射角度
        public float SpreadAngle;
        //每秒射速
        public float FireRate;
        //水平后坐力
        public float HorizontalRecoil;
        //垂直后坐力
        public float VerticalRecoil;
        //震屏幅度绝对值
        public float ScreenSpringRange;
        //震屏回正速度
        public float DampSpeed = 15f;
        //后坐力曲线
        public AnimationCurve RecoilCurve;
    }
}