using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Weapon
{
    //根据玩家操作集定义武器要实现的接口，武器中没有的可以直接return
    public interface IWeapon
    {
        //左键按下
        void HoldMainTrigger();
        //左键松开
        void ReleaseMainTrigger();
        //右键点按（次要扳机）
        void HoldSubTrigger();
        //右键松开
        void ReleaseSubTrigger();
        //开始充能
        void StartCharge();
        //进行攻击
        void DoAttack();
        //右键按下（瞄准）
        void Aim();
        //刺刀冲锋
        void SprintMelee();
        //武器能力
        void WeaponAbility(bool onOff);
        //切换开火模式
        void SwitchFireMode();
        //切换归零点
        void SwitchZeroPoint();
    }
    
    //步枪、霰弹枪、机枪、冲锋枪、狙击枪、手枪、榴弹枪、火炮、高射炮、火箭炮
    public enum WeaponType
    {
        AssualtRifle,
        ShotGun,
        MachineGun,
        SMGun,
        Sniper,
        HandGun,
        Grenade,
        Cannon,
        AAGun,
        Rocket,
    }

    //自动、三连发、单发、单发栓动式(栓动式单独写类，充能也后面单独做，单独写个类而非开火模式)
    public enum FireMode
    {
        Auto,
        ThreeBurst,
        Semiautomatic,
        BoltAction
    }
}