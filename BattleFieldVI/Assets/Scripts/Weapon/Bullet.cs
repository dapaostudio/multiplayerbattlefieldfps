﻿using System.Collections;
using UnityEngine;

namespace Assets.Scripts.Weapon
{
    public class Bullet : MonoBehaviour
    {
        //子弹伤害表，由Firearm指定
        public WeaponDamageValue WeaponDamage;
        //子弹所属玩家名，由Firearm指定
        public string PlayerName;
        //子弹所属阵营，由Firearm指定，阵营影响伤害效果
        public Faction BulletFaction;
        //子弹类型，手动绑定
        public BulletType BulletType;
        //子弹击中特效，手动绑定
        public GameObject ImpactPrefab;
        //子弹击中音效，手动绑定
        public ImpactAudioData ImpactAudioData;
        //子弹生命周期，用于爆炸物的时间引信或子弹销毁，手动绑定
        public float BulletLifeTime = 10f;
        //子弹近炸检测范围，应该是写子类并用碰撞体Enter来触发，TODO

        //子弹上一帧位置
        protected Vector3 prevPosition;

        protected float verticalSpeed = 0f;
        protected Vector3 horizontalVelocity;
        protected float gravity = Mathf.Abs(Physics.gravity.y);

        protected virtual void Awake()
        {
            //生命周期后定时销毁
            Destroy(gameObject, BulletLifeTime + 0.1f);
            //更新初始位置
            prevPosition = transform.position;
        }

        protected virtual void FixedUpdate()
        {
            BulletMove();
            CheckHit();
        }

        void LateUpdate()
        {
            //一帧的最后更新位置
            prevPosition = transform.position;
        }

        //装配子弹
        public void SetupBullet(WeaponDamageValue weaponDamage, string playerName, Faction faction)
        {
            WeaponDamage = weaponDamage;
            PlayerName = playerName;
            BulletFaction = faction;
        }

        public void SetUpBulletSpeed(float initialVerticalSpeed, Vector3 initialHorizontalVelocity)
        {
            verticalSpeed = initialVerticalSpeed;
            horizontalVelocity = initialHorizontalVelocity;
        }

        //移动子弹
        public void BulletMove()
        {
            // 计算垂直位移
            float verticalDisplacement = verticalSpeed * Time.deltaTime - 0.5f * gravity * Mathf.Pow(Time.deltaTime, 2);
            verticalSpeed -= gravity * Time.deltaTime;

            // 计算水平位移
            Vector3 horizontalDisplacement = horizontalVelocity * Time.deltaTime;

            // 应用位移
            transform.Translate(horizontalDisplacement + Vector3.up * verticalDisplacement, Space.World);
        }

        //检查子弹命中
        public virtual void CheckHit()
        {
            //检测这一帧是否命中目标
            if (!Physics.Raycast(prevPosition,
                (transform.position - prevPosition).normalized,
                out RaycastHit Hit,
                (transform.position - prevPosition).magnitude)) return;
            Debug.DrawRay(transform.position, (transform.position - prevPosition).normalized, Color.red, 1f);
            //尝试获取命中的HitBox
            if (Hit.transform.TryGetComponent(out BodyPart bodyPart))
            {
                //如果命中的是士兵
                if(bodyPart.ArmorType == ArmorType.Human)
                {
                    //如果是己方士兵则直接返回
                    if (bodyPart.PartFaction == BulletFaction) return;
                }
            }
            //否则命中敌方士兵、装甲单位、其他碰撞体
            OnHit(Hit);
        }

        //子弹命中时执行
        public virtual void OnHit(RaycastHit hit)
        {
            //生成击中特效
            GameObject BulletImpactEffect = Instantiate(ImpactPrefab, hit.point, Quaternion.LookRotation(hit.normal, Vector3.up));
            Destroy(BulletImpactEffect, 2f);
            //生成击中音效，匹配击中的Tag
            ImpactTagsWithAudio TagsWithAudio = ImpactAudioData.ImpactTagsWithAudios.Find(t => hit.collider.CompareTag(t.Tag));
            //如果匹配到就随机击中音效，并挂载在击中特效上
            if (TagsWithAudio != null)
            {
                AudioClip audioClip = TagsWithAudio.ImpactAudioClips[Random.Range(0, TagsWithAudio.ImpactAudioClips.Count)];
                AudioManager.PlaySFX3D(audioClip, BulletImpactEffect.transform);
            }
            //造成伤害（传递hit是为了方便计算爆炸范围等）
            OnDamage(hit);
        }



        //子弹造成伤害时执行
        public virtual void OnDamage(RaycastHit hit)
        {
            //TODO
            Debug.Log("子弹命中了，该写伤害了");
            Destroy(gameObject);
        }
    }
}