﻿using Cinemachine;
using RootMotion.Demos;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

namespace Assets.Scripts.Weapon
{
    //各类以子弹为伤害手段的枪械类基类，例如步枪、手枪、冲锋枪、狙击枪。霰弹枪、坦克火炮和固定武器也可以考虑一下
    //暂时没做多种弹药的配置，如果做可以从BulletSpeed和BulletPrefab入手，同时对后坐力和扩散做出区分TODO
    public abstract class Firearms : MonoBehaviour, IWeapon
    {
        /*—————————————————————————手绑参数——————————————————————————*/
        //枪械Data，使用Data初始化固有参数，绑完连着手臂做成预制体
        public FirearmData FirearmData;
        //默认枪口位置
        public Transform MuzzlePoint;
        //默认开火特效位置
        public Transform MuzzleParticlePoint;
        //抛弹特效
        public ParticleSystem CasigParticle;
        //默认瞄具父物体
        public Transform ScopeRoot;
        //默认弹夹父物体
        public Transform MagazineRoot;
        //默认下挂父物体
        public Transform BarrelAttachRoot;
        //默认枪管父物体
        public Transform BarrelRoot;
        /*—————————————————————————固有参数——————————————————————————*/
        //全部放进FirearmData里了
        /*————————————————————————运行时参数——————————————————————————*/
        //视角虚拟相机，从上层Awake时传入
        public CinemachineVirtualCamera EyeCamera;
        //瞄准相机
        public Camera GunCamera;
        //枪械动画控制器
        public Animator GunAnimator;
        //当前枪口位置，通过默认位置计算而来
        protected Vector3 currentMuzzlePosition;
        //当前装配的瞄具
        protected ScopeInfo rigoutScopeInfo;
        //当前使用的弹夹配置
        protected MagazineInfo rigoutMagzineInfo;
        //当前使用的枪管配置
        protected BarrelInfo rigoutBarrelInfo;
        //当前使用的下挂配置
        protected BarrelAttachInfo rigoutBarrelAttachInfo;
        //当前弹夹与当前备弹
        protected int currentAmmo;
        protected int currentRemainAmmoCarried;
        //当前开火模式
        protected FireMode CurrentFireMode;
        //上次开火时间，用于检测是否可以发射
        protected float lastFireTime;
        //是否正在换弹
        public bool IsReloading { get => isReloading; }
        private bool isReloading = false;
        //剩余可连发子弹数，连发模式为当前剩余子弹数，爆炸开火为3，半自动和栓动式为1，
        protected int RemainTriggerCount;
        //剩余可不换弹子弹数，非栓动式和剩余弹夹子弹数一致，栓动式为1
        protected int RemainNeednotReloadCount;
        //当前热度，过热后需要散热
        protected int CurrentHeat;
        //开火与开镜
        public bool IsFire;
        public bool IsAiming { get => isAiming; }
        protected bool isAiming = false;
        //是否在按住扳机
        protected bool isHoldingTrigger = false;
        //换弹检测用动画检测器
        protected AnimatorStateInfo GunStateInfo;
        //摄像机FOV
        protected float EyeOriginFOV;
        protected float GunOriginFOV;
        //瞄准协程
        private IEnumerator doAimCoroutine;
        //摄像机坐标
        private Vector3 originEyePostion;
        //枪械摄像机Transform
        private Transform gunCameraTransform;
        //当前瞄准镜
        private GameObject currentScopeObject;
        //当前弹夹
        private GameObject currentMagzineObject;
        //当前枪管
        private GameObject currentBarrelObject;
        //当前枪管下挂
        private GameObject currentBarrelAttachObject;
        //当前归零点
        protected float currentZeroPoint;
        //当前枪械开火属性
        public RuntimeInfoForFire RuntimeFireConfig;
        //当前已连续开火子弹数
        protected int continuousFireCount;
        //当前后坐力偏移具体数值
        private Vector2 currentRecoil;
        //当前后坐力时间轴
        private float currentRecoilTime;
        //开火粒子特效
        protected ParticleSystem MuzzleParticle;
        /*————————————————————————新属性——————————————————————————*/
        //TODO换弹速度
        //对WeaponManager的引用，一般来说丢弃时需要解绑TODO
        public WeaponManager WeaponManager;
        protected virtual void Awake()
        {
            //生成开火特效
            GameObject muzzleParticle = Instantiate(FirearmData.MuzzleParticle.gameObject, MuzzleParticlePoint.position, MuzzleParticlePoint.rotation);
            muzzleParticle.transform.SetParent(MuzzleParticlePoint.transform);
            MuzzleParticle = muzzleParticle.GetComponent<ParticleSystem>();
            //安装默认的武器配件
            InitWeaponAttach();
            //初始化RuntimeFireConfig用于开火调用
            InitRuntimeFireConfig();
            //获取Animator
            GunAnimator = GetComponent<Animator>();

        }

        protected virtual void Start()
        {
            //初始化运行时数据
            InitRuntimeConfig();
            //初始化开火模式
            InitFireModeConfig();
        }
        
        protected virtual void OnEnable()
        {
            UpdateFireConfig();
        }
        protected virtual void FixedUpdate()
        {
            //每帧均进行后坐力插值并反馈给SoldierControl
            UpdateRecoliOffset();
            //检测是否持续开火
            if (IsFire) HoldMainTrigger();
        }

        //在初始化加载或捡起武器时，对Camera进行赋值并初始化摄像机数据
        public void InitCameraFromHighLevel(CinemachineVirtualCamera eyeCamera, Camera gunCamera)
        {
            //绑定摄像机
            EyeCamera = eyeCamera;
            GunCamera = gunCamera;
            //初始化摄像机信息
            InitCameraConfig();
        }

        //从固有属性初始化运行时数据
        void InitRuntimeConfig()
        {

            CurrentHeat = 0;
            doAimCoroutine = DoAim();

            //默认为第一个归零点
            currentZeroPoint = FirearmData.ZeroPointInfo[0];
        }
        //初始化摄像机Transform与FOV
        void InitCameraConfig()
        {
            gunCameraTransform = GunCamera.transform;
            originEyePostion = gunCameraTransform.localPosition;

            EyeOriginFOV = EyeCamera.m_Lens.FieldOfView;
            GunOriginFOV = GunCamera.fieldOfView;
        }

        //更新实时后坐力偏移值
        protected void UpdateRecoliOffset()
        {
            //计算并插值每帧后坐力偏移值
            currentRecoilTime += Time.fixedDeltaTime;
            float currentRecoilTimeFraction = currentRecoilTime / RuntimeFireConfig.RecoilFadeTime;
            float recoilFraction = RuntimeFireConfig.RecoilCurve.Evaluate(currentRecoilTimeFraction);
            currentRecoil = Vector2.Lerp(Vector2.zero,currentRecoil, recoilFraction);
            //向SoldierControl输出后坐力
            WeaponManager.player.Soldier.GetComponent<Soldier>().SoldierControl.GetRecoilOffset(currentRecoil.x, currentRecoil.y);
        }
        //开火时执行后坐力
        protected void RecoilWhenFire()
        {
            //计算连续开火导致枪支不可控性比例
            float fireControlFraction = continuousFireCount / (float)FirearmData.MaxControlShotCount;
            //瞄准状态的后坐力比例降低
            float recoilAimFraction = isAiming ? RuntimeFireConfig.RecoilAimFraction : 1f;
            //水平后坐力
            currentRecoil.x += RuntimeFireConfig.HorizontalRecoil * FirearmData.ControlCurve.Evaluate(fireControlFraction) * recoilAimFraction;
            //垂直后坐力
            currentRecoil.y += RuntimeFireConfig.VerticalRecoil * FirearmData.ControlCurve.Evaluate(fireControlFraction) * recoilAimFraction;
            //重置后坐力时间以重新开始后坐力
            currentRecoilTime = 0f;
            //震屏
            WeaponManager.player.CameraSpring.StartCameraSpring();

        }

        //剩余可连发子弹数与不换弹子弹数
        void InitFireModeConfig()
        {
            if (FirearmData.SupportFireModes.Contains(FireMode.Auto))
            {
                RemainTriggerCount = rigoutMagzineInfo.AmmoInMag;
                RemainNeednotReloadCount = RemainTriggerCount;
                CurrentFireMode = FireMode.Auto;
            }
            else if (FirearmData.SupportFireModes.Contains(FireMode.ThreeBurst))
            {
                RemainTriggerCount = 3;
                RemainNeednotReloadCount = rigoutMagzineInfo.AmmoInMag;
                CurrentFireMode = FireMode.ThreeBurst;
            }
            else if (FirearmData.SupportFireModes.Contains(FireMode.Semiautomatic))
            {
                RemainTriggerCount = 1;
                RemainNeednotReloadCount = rigoutMagzineInfo.AmmoInMag;
                CurrentFireMode = FireMode.Semiautomatic;
            }
            else
            {
                RemainTriggerCount = 1;
                RemainNeednotReloadCount = 1;
                CurrentFireMode = FireMode.BoltAction;
            }
        }
        //重置武器发射模式
        protected void ResetFireMode(FireMode fireMode)
        {
            CurrentFireMode = fireMode;
            switch (fireMode)
            {
                case FireMode.Auto:
                    RemainTriggerCount = rigoutMagzineInfo.AmmoInMag;
                    RemainNeednotReloadCount = RemainTriggerCount;
                    break;
                case FireMode.ThreeBurst:
                    RemainTriggerCount = 3;
                    RemainNeednotReloadCount = rigoutMagzineInfo.AmmoInMag;
                    break;
                case FireMode.Semiautomatic:
                    RemainTriggerCount = 1;
                    RemainNeednotReloadCount = rigoutMagzineInfo.AmmoInMag;
                    break;
                case FireMode.BoltAction: 
                    RemainTriggerCount = 1;
                    RemainNeednotReloadCount = 1;
                    break;
                default:
                    break;
            }
        }
        //为武器安装瞄准镜
        public void SetUpScope(ScopeInfo scope)
        {
            //销毁旧镜头，或许日后可以用更好的方式，但这里先销毁TODO
            if (currentScopeObject != null) Destroy(currentScopeObject);
            //实例化镜头并挂接在ScopeRoot下
            currentScopeObject = Instantiate(scope.ScopeGameObject);
            currentScopeObject.transform.SetParent(ScopeRoot);
            //修改镜头本地坐标和旋转
            currentScopeObject.transform.localPosition = scope.ScopesOnFirearms.Find(t => t.FirearmName == FirearmData.FirearmName).Position;
            currentScopeObject.transform.localRotation = scope.ScopesOnFirearms.Find(t => t.FirearmName == FirearmData.FirearmName).Rotation;
            //修改当前镜头info
            rigoutScopeInfo = scope;
        }

        //为武器安装弹夹
        public void SetUpMagazine(MagazineInfo magazine) 
        {
            //销毁旧弹夹，或许日后可以用更好的方式，但这里先销毁TODO
            if(currentMagzineObject != null) Destroy(currentMagzineObject);
            //实例化弹夹并挂接在MagazineRoot下
            currentMagzineObject = Instantiate(magazine.MagazineGameObject);
            currentMagzineObject.transform.SetParent(MagazineRoot);
            //修改弹夹本地坐标和旋转
            currentMagzineObject.transform.localPosition = magazine.MagazineOnFirearms.Find(t => t.FirearmName == FirearmData.FirearmName).Position;
            currentMagzineObject.transform.localRotation = magazine.MagazineOnFirearms.Find(t => t.FirearmName == FirearmData.FirearmName).Rotation;
            //修改当前弹夹Info
            rigoutMagzineInfo = FirearmData.DefaultMagazine;
        }
        //为武器安装枪管
        public void SetUpBarrel(BarrelInfo barrel) 
        {
            //销毁旧枪管，或许日后可以用更好的方式，但这里先销毁TODO
            if (currentBarrelObject != null) Destroy(currentBarrelObject);
            //实例化枪管并挂接在BarrelRoot下
            currentBarrelObject = Instantiate(barrel.BarrelGameObject);
            currentBarrelObject.transform.SetParent(BarrelRoot);
            //修改枪管本地坐标和旋转
            currentBarrelObject.transform.localPosition = barrel.BarrelOnFirearms.Find(t => t.FirearmName == FirearmData.FirearmName).Position;
            currentBarrelObject.transform.localRotation = barrel.BarrelOnFirearms.Find(t => t.FirearmName == FirearmData.FirearmName).Rotation;
            //修改当前枪管Info
            rigoutBarrelInfo = FirearmData.DefaultBarrel;
            //Debug.Log("111");
            //修改当前枪口坐标与枪口特效坐标
            currentMuzzlePosition = MuzzlePoint.transform.position + barrel.DeltaMuzzlePointOffset;
            MuzzleParticle.transform.position = MuzzleParticlePoint.transform.position + barrel.DeltaMuzzlePointOffset;

        }
        //为武器安装枪管配件
        public void SetUpBarrelAttach(BarrelAttachInfo barrelAttach)
        {
            //销毁旧枪管配件，或许日后可以用更好的方式，但这里先销毁TODO
            if (currentBarrelAttachObject != null) Destroy(currentBarrelAttachObject);
            //实例化枪管配件并挂接在BarrelRoot下
            currentBarrelAttachObject = Instantiate(barrelAttach.BarrelAttachGameObject);
            currentBarrelAttachObject.transform.SetParent(BarrelAttachRoot);
            //修改枪管配件本地坐标和旋转
            currentBarrelAttachObject.transform.localPosition = barrelAttach.BarrelAttachOnFirearms.Find(t => t.FirearmName == FirearmData.FirearmName).Position;
            currentBarrelAttachObject.transform.localRotation = barrelAttach.BarrelAttachOnFirearms.Find(t => t.FirearmName == FirearmData.FirearmName).Rotation;
            //修改当前枪管配件Info
            rigoutBarrelAttachInfo = FirearmData.DefaultBarrelAttach;
        }
        //初始化武器配件GameObject
        public void InitWeaponAttach()
        {
            //安装默认弹夹
            SetUpMagazine(FirearmData.DefaultMagazine);
            //安装默认枪管
            SetUpBarrel(FirearmData.DefaultBarrel);
            //安装默认枪管配件
            SetUpBarrelAttach(FirearmData.DefaultBarrelAttach);
            //安装默认瞄具
            SetUpScope(FirearmData.DefaultScope);
        }
        //初始化开火配置数据
        public void InitRuntimeFireConfig()
        {
            UpdateFireConfig();
        }
        //更新运行时开火配置数据
        public void UpdateFireConfig()
        {
            //从FirearmData里获取武器基底数据
            RuntimeFireConfig = FirearmData.WeaponBaseFireConfig;
            //从弹夹里获取子弹类型
            RuntimeFireConfig.CurrentBulletType = rigoutMagzineInfo.BulletType;
            //通过子弹类型从武器-子弹表里获取伤害数值与伤害曲线
            RuntimeFireConfig.CurrentWeaponDamageConfig = FirearmData.WeaponDamageInfoTable.Find(t => t.BulletType == RuntimeFireConfig.CurrentBulletType).DamageValue;
            
            //从当前弹夹获取当前弹夹子弹数、备弹、射速
            if(rigoutMagzineInfo.AmmoCarried>= rigoutMagzineInfo.AmmoInMag)
            {   //如果携带的子弹数量大于弹夹容量，就把子弹装进弹夹，然后把携带的子弹扣除装进去的子弹数
                currentAmmo = rigoutMagzineInfo.AmmoInMag;
            }
            else
            {   //否则把剩余子弹都装进去，这样是为了实现“用过的枪”这一功能
                currentAmmo = rigoutMagzineInfo.AmmoCarried;
            }
            currentRemainAmmoCarried = rigoutMagzineInfo.AmmoCarried - currentAmmo;
            RuntimeFireConfig.FireRate = rigoutMagzineInfo.RPM / 60;

            //从各个配件的改动值来修改RuntimeFireConfig里的后坐力等
            DeltaFireConfigFromAttach(rigoutMagzineInfo.WeaponAttachDelta);
            DeltaFireConfigFromAttach(rigoutBarrelInfo.WeaponAttachDelta);
            DeltaFireConfigFromAttach(rigoutBarrelAttachInfo.WeaponAttachDelta);

            //更新震屏组件值
            WeaponManager.player.CameraSpring.SetSpringRangeAndFrequence(RuntimeFireConfig.ScreenSpringRange, RuntimeFireConfig.DampSpeed);
        }

        //通过配件影响枪械性能数值
        public void DeltaFireConfigFromAttach(WeaponAttachDelta weaponAttachDelta)
        {
            RuntimeFireConfig.CurrentWeaponDamageConfig.SoldierDamage += weaponAttachDelta.DeltaSoldierDamage;
            RuntimeFireConfig.CurrentWeaponDamageConfig.LightArmorDamage += weaponAttachDelta.DeltaLightArmorDamage;
            RuntimeFireConfig.CurrentWeaponDamageConfig.HeavyArmorDamage += weaponAttachDelta.DeltaHeavyArmorDamage;
            RuntimeFireConfig.RecoilAimFraction += weaponAttachDelta.DeltaRecoilAimFraction;
            RuntimeFireConfig.RecoilFadeTime += weaponAttachDelta.DeltaRecoliFadeTime;
            RuntimeFireConfig.BulletSpeed += weaponAttachDelta.DeltaBulletSpeed;
            RuntimeFireConfig.SpreadAngle += weaponAttachDelta.DeltaSpreadAngle;
            RuntimeFireConfig.FireRate += weaponAttachDelta.DeltaFireRate;
            RuntimeFireConfig.HorizontalRecoil += weaponAttachDelta.DeltaHorizontalRecoil;
            RuntimeFireConfig.VerticalRecoil += weaponAttachDelta.DeltaVerticalRecoilRange;
            RuntimeFireConfig.ScreenSpringRange += weaponAttachDelta.DeltaScreenSpringRange;
            RuntimeFireConfig.DampSpeed += weaponAttachDelta.DeltaDampSpeed;
        }
        protected abstract void Shooting();
        protected abstract void Reload();
        //检测是否可以下一次射击
        protected virtual bool IsAllowShooting()
        {
            if (currentAmmo <= 0) return false;
            if (RemainTriggerCount <= 0) return false;
            if (RemainNeednotReloadCount <= 0) return false;
            return Time.time - lastFireTime > 1f / RuntimeFireConfig.FireRate;
        }

        //计算散射，可能需要在开镜时特殊处理TODO
        protected Vector3 CalculateSpreadOffset()
        {
            float SpreadPercent = RuntimeFireConfig.SpreadAngle / EyeCamera.m_Lens.FieldOfView;
            return Random.insideUnitCircle * SpreadPercent;
        }

        public virtual void Aim()
        {
            if (IsReloading) return;
            Aiming(true);
        }

        public virtual void DoAttack()
        {
            if (IsAllowShooting())
            {
                Shooting();
            }
        }

        public virtual void ReloadAmmo()
        {
            if (IsReloading && currentRemainAmmoCarried <= 0) return;
            Reload();
        }

        public virtual void HoldMainTrigger()
        {
            if (IsReloading) return;
            DoAttack();
            continuousFireCount += 1;
            isHoldingTrigger = true;
            IsFire = true;
        }

        public virtual void HoldSubTrigger()
        {
            return;
            //次要扳机Tap动作暂时不做，子类再考虑TODO
        }

        public void ReleaseMainTrigger()
        {
            if (IsReloading) return;
            isHoldingTrigger = false;
            IsFire = false;
            continuousFireCount = 0;
            ResetFireMode(CurrentFireMode);
        }

        public virtual void ReleaseSubTrigger()
        {
            if (IsReloading) return;
            Aiming(false);
        }

        public virtual void SprintMelee()
        {
            return;
            //FireArm基类暂时不实现刺刀冲锋，可以子类实现TODO
        }

        public virtual void StartCharge()
        {
            return;
            //FireArm基类暂时不实现充能步枪，可以子类实现TODO
        }
        //按键轮流切换开火模式
        public virtual void SwitchFireMode()
        {
            // 如果SupportFireModes为空或只有一个元素，直接返回
            if (FirearmData.SupportFireModes == null || FirearmData.SupportFireModes.Count <= 1)
                return;
            // 查找CurrentFireMode在SupportFireModes中的索引
            int index = FirearmData.SupportFireModes.IndexOf(CurrentFireMode);
            // 如果没找到CurrentFireMode，或者CurrentFireMode是列表中的最后一个元素
            if (index == -1 || index == FirearmData.SupportFireModes.Count - 1)
            {
                // 将CurrentFireMode设置为列表中的第一个元素
                CurrentFireMode = FirearmData.SupportFireModes[0];
            }
            else
            {
                // 否则，将CurrentFireMode设置为列表中的下一个元素
                CurrentFireMode = FirearmData.SupportFireModes[index + 1];
            }
            //如果不是按住扳机，即重置连续开火弹药
            if (isHoldingTrigger == false) ResetFireMode(CurrentFireMode);
        }

        public virtual void SwitchZeroPoint()
        {
            //如果只有一种归零点就直接返回
            if (FirearmData.ZeroPointInfo.Count == 1) return;
            //FireArm基类暂时不实现归零点，可以子类实现
        }

        public virtual void WeaponAbility(bool onOff)
        {
            return;
            //FireArm基类暂时不实现武器技能，可以子类实现
        }

        //计算子弹发射角度
        protected float CalculateLaunchAngle()
        {
            float gravity = Mathf.Abs(Physics.gravity.y);
            // 利用二次方程求解公式，找到使得子弹在水平位移为zeroPoint时高度为0的垂直分速度
            float discriminant = Mathf.Pow(RuntimeFireConfig.BulletSpeed, 4) - gravity * (gravity * Mathf.Pow(currentZeroPoint, 2));

            if (discriminant < 0)
            {
                // 没有解，意味着给定的bulletSpeed和zeroPoint下，子弹无法回到0高度
                return 0f;
            }

            float rootDiscriminant = Mathf.Sqrt(discriminant);

            // 可能存在两个解，我们选择较小的那个，因为我们想要的是最小发射角度
            float verticalSpeed = (Mathf.Pow(RuntimeFireConfig.BulletSpeed, 2) - rootDiscriminant) / (2 * gravity);

            // 通过勾股定理计算出水平分速度
            float horizontalSpeed = Mathf.Sqrt(Mathf.Pow(RuntimeFireConfig.BulletSpeed, 2) - Mathf.Pow(verticalSpeed, 2));

            // 发射角度 = atan(垂直分速度 / 水平分速度)
            float launchAngle = Mathf.Atan(verticalSpeed / horizontalSpeed);

            // 将角度从弧度转换为度
            return launchAngle * Mathf.Rad2Deg;
        }

        // 逻辑装弹，改变备弹与弹夹子弹，步枪类实现了弹夹内有子弹时换弹保留枪膛子弹。虚函数以便于继承本类扩展狙击枪类
        protected virtual void DoReload()
        {
            int ammoToReload = currentAmmo == 0 ? rigoutMagzineInfo.AmmoInMag : (rigoutMagzineInfo.AmmoInMag + 1) - currentAmmo; // 计算需要装填的子弹数量，包括枪膛内的一发子弹

            if (currentRemainAmmoCarried >= ammoToReload)
            {
                currentAmmo += ammoToReload; // 装填子弹
                currentRemainAmmoCarried -= ammoToReload; // 减少剩余备弹
            }
            else
            {
                currentAmmo += currentRemainAmmoCarried; // 当备弹不足时，装填剩余的所有子弹
                currentRemainAmmoCarried = 0; // 剩余备弹归零
            }
            //按照当前射击模式重置三连发或半自动余量
            ResetFireMode(CurrentFireMode);
            isReloading = false;
        }

        // 检测装弹动画播完时搞定装弹并退出的协程，虚函数以便于继承本类扩展狙击枪类
        protected virtual IEnumerator Reloading()
        {
            //根据装弹速度来调整Animator速度
            isReloading = true;
            while (true)
            {
                GunStateInfo = GunAnimator.GetCurrentAnimatorStateInfo(2);
                if (GunStateInfo.IsTag("ReloadAmmo"))
                {
                    if (GunStateInfo.normalizedTime > 0.85f)
                    {
                        DoReload();
                        GunAnimator.SetLayerWeight(2, 0);
                        yield break;
                    }
                }
                yield return null;
            }

        }

        //进入和离开瞄准状态
        internal void Aiming(bool onOff)
        {
            isAiming = onOff;
            GunAnimator.SetBool("Aim", isAiming);
            if (doAimCoroutine == null)
            {
                doAimCoroutine = DoAim();
                StartCoroutine(doAimCoroutine);
            }
            else
            {
                StopCoroutine(doAimCoroutine);
                doAimCoroutine = null;
                doAimCoroutine = DoAim();
                StartCoroutine(doAimCoroutine);
            }
        }
        
        //瞄准协程，用于平滑拉近镜头并平滑改变FOV
        protected IEnumerator DoAim()
        {
            while (true)
            {
                yield return null;

                float EyeCurrentFov = 0;
                EyeCamera.m_Lens.FieldOfView = Mathf.SmoothDamp(EyeCamera.m_Lens.FieldOfView,
                    isAiming ? rigoutScopeInfo.EyeFov : EyeOriginFOV,
                    ref EyeCurrentFov,
                    Time.deltaTime * 2);

                float GunCurrentFov = 0;
                GunCamera.fieldOfView = Mathf.SmoothDamp(GunCamera.fieldOfView,
                    isAiming ? rigoutScopeInfo.GunFov : GunOriginFOV,
                    ref GunCurrentFov,
                    Time.deltaTime * 2);

                Vector3 RefPostion = Vector3.zero;
                gunCameraTransform.localPosition = Vector3.SmoothDamp(gunCameraTransform.localPosition,
                    isAiming ? rigoutScopeInfo.GunCameraPostion : originEyePostion,
                    ref RefPostion,
                    Time.deltaTime * 2);
            }
        }
    }
}