﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Weapon
{
    //统筹管理所有的武器，但目前只实现了步兵状态，其他状态暂时未实现TODO
    //暂时不做丢弃武器与捡起武器以及载具武器管理的功能，后续再说TODO
    //武器必须按照顺序加入，且不可为空
    public class WeaponManager : MonoBehaviour
    {
        //Player引用，手动拖进来即可
        public Player player;
        //武器字典
        public List<Firearms> Weapons;
        //当前武器索引
        public int CurrentWeaponIndex;
        //当前武器
        public Firearms CarriedWeapon { get => Weapons[CurrentWeaponIndex]; }
        //携带的武器预制体列表，开始时从该列表实例化并设置武器，后续可以通过武器Slot来捡起武器
        public List<GameObject> WeaponPrefabs;
        //武器挂载点列表，用于在实例化武器后挂载，暂且为五个武器，且目前挂载点均在Soldier下，后续扩展再看看怎么做载具TODO
        public List<Transform> WeaponHolders;
        //切枪协程
        private IEnumerator waitingForHolsterEnd;


        // Use this for initialization
        void Start()
        {
            InitWeapon();
        }

        // Update is called once per frame
        void Update()
        {

        }
        
        //通过滚轮前后切换武器
        public void SwapWeapon(int delta)
        {
            //正在播放收枪动画时无法切武器
            if (waitingForHolsterEnd != null) return;
            //确保delta只有-1,0,1三种取值
            delta = Math.Sign(delta);
            //更新目标武器索引
            int targetIndex = CurrentWeaponIndex + delta;
            //确保更换武器的索引是循环的
            if (targetIndex < 0)
            {
                targetIndex = Weapons.Count - 1;
            }
            else if (targetIndex >= Weapons.Count)
            {
                targetIndex = 0;
            }
            //切换到指定武器
            SwapToWeapon(targetIndex);
        }

        //通过数字键快速切换武器
        public void SwapToWeapon(int targetIndex)
        {
            //正在播放收枪动画时无法切武器
            if (waitingForHolsterEnd != null) return;
            //确保快速切换武器的索引值是合法的
            if (targetIndex >= 0 && targetIndex < Weapons.Count)
            {
                //当前武器播放收枪动作
                CarriedWeapon.GunAnimator.SetTrigger("Holster");
                //执行更换武器操作
                DoSwapWeapon(targetIndex);
            }
        }

        //执行换武器协程
        void DoSwapWeapon(int targetIndex)
        {
            if (waitingForHolsterEnd == null)
            {
                waitingForHolsterEnd = WaitingForHolsterEnd(targetIndex);
            }
            StartCoroutine(waitingForHolsterEnd);
        }

        //切换武器前，等待播放收枪动画
        private IEnumerator WaitingForHolsterEnd(int targetIndex)
        {
            while (true)
            {
                AnimatorStateInfo animatorStateInfo = CarriedWeapon.GunAnimator.GetCurrentAnimatorStateInfo(0);
                if (animatorStateInfo.IsTag("Holster"))
                {
                    //当正在播放的是收枪动画且将要播完时
                    if(animatorStateInfo.normalizedTime > 0.95f)
                    {
                        //设置武器为目标武器
                        SetUpCarriedWeapon(targetIndex);
                        waitingForHolsterEnd = null;
                        break;
                    }
                }
            }
            yield return null;
        }

        //更换并启用武器
        private void SetUpCarriedWeapon(int targetIndex)
        {
            if(CarriedWeapon!=null)
            {
                //隐藏被换掉的武器
                CarriedWeapon.gameObject.SetActive(false);
            }
            CurrentWeaponIndex = targetIndex;
            //打开新武器
            CarriedWeapon.gameObject.SetActive(true);
            //更换Soldier的AnimatorController
            if (player.IsCurrentOnlyHuman)
            {
                player.Soldier.GetComponent<Soldier>().CurrentAnimation = CarriedWeapon.GunAnimator;
            }
        }

        //重生时，初始化武器
        private void InitWeapon()
        {
            Weapons.Clear();
            //加载全部的武器
            for (int i = 0; i < WeaponPrefabs.Count; i++)
            {
                Firearms firearm = LoadWeaponPrefab(WeaponPrefabs[i]);
                //在武器列表的特定槽位中加入该武器
                Weapons.Add(firearm);
                //为武器绑定摄像机，这里没有支持载具的武器，因为载具存在更多摄像机且存在视野切换，后续看看怎么支持TODO
                firearm.InitCameraFromHighLevel(player.PlayerViewCamera, player.PlayerGunCamera);
                //为武器的WeaponManager进行赋值
                firearm.WeaponManager = this;
                //暂时隐藏武器
                firearm.gameObject.SetActive(false);
            }
            //拿出第一把武器,这里默认是携带了主武器的，如果没有主武器就G，后续看看怎么做容错TODO
            SetUpCarriedWeapon(0);
        }

        //从预制体实例化武器预制体并挂载到对应点位
        private Firearms LoadWeaponPrefab(GameObject weaponPrefab)
        {
            //从预制体实例化武器预制体
            GameObject weapon = Instantiate(weaponPrefab);
            //从武器内获知武器槽位
            int index = (int)weapon.GetComponent<Firearms>().FirearmData.WeaponSlot;
            //将武器加载到对应槽位下
            weapon.transform.SetParent(WeaponHolders[index], false);
            //返回武器的Firearms类
            return weapon.GetComponent<Firearms>();
        }
    }
}