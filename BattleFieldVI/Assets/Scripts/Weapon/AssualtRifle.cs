﻿using System.Collections;
using UnityEngine;

namespace Assets.Scripts.Weapon
{
    //步枪类，一切打正常子弹的都可以使用这个类
    public class AssualtRifle : Firearms
    {
        protected override void Reload()
        {
            IsFire = false;
            //没子弹就不要装弹了
            if (currentRemainAmmoCarried <= 0) return;
            //播放装弹动画
            GunAnimator.SetLayerWeight(2, 1);
            GunAnimator.SetTrigger(currentAmmo > 0 ? "ReloadLeft" : "ReloadOutOf");
            //播放装弹音效
            AudioManager.PlaySFX3D(currentAmmo > 0 ? FirearmData.FirearmsAudioData.ReloadLeftAudio : FirearmData.FirearmsAudioData.ReloadOutOfAudio, transform);
            //开始装弹协程
            StartCoroutine(Reloading());
        }

        protected override void Shooting()
        {
            //播放特效
            MuzzleParticle.Play();
            CasigParticle.Play();
            //播放动画
            GunAnimator.Play("Fire", isAiming ? 1 : 0);
            int index = Random.Range(0, FirearmData.FirearmsAudioData.ShootingAudios.Count);
            //播放音效
            AudioManager.PlaySFX3D(FirearmData.FirearmsAudioData.ShootingAudios[index], MuzzlePoint);
            //生成并发射子弹
            CreateBullet();
            //执行开火后坐力
            RecoilWhenFire();
            //减少子弹数
            currentAmmo -= 1;
            RemainTriggerCount -= 1;
            RemainNeednotReloadCount -= 1;
            //记录发射时间
            lastFireTime = Time.time;
        }

        protected void CreateBullet()
        {
            GameObject bullet;
            if (IsAiming)
            {
                //瞄准状态从摄像机中心射出，且无扩散
                bullet = Instantiate(rigoutMagzineInfo.BulletGameObject, EyeCamera.transform.position, EyeCamera.transform.rotation * Quaternion.Euler(-CalculateLaunchAngle(), 0, 0));
            }
            else
            {
                //腰射状态从枪口射出，且有扩散
                bullet = Instantiate(rigoutMagzineInfo.BulletGameObject, MuzzlePoint.position, MuzzlePoint.rotation * Quaternion.Euler(-CalculateLaunchAngle(), 0, 0));
                bullet.transform.eulerAngles += CalculateSpreadOffset();
            }
            //设置子弹属性
            bullet.GetComponent<Bullet>().SetupBullet(RuntimeFireConfig.CurrentWeaponDamageConfig, WeaponManager.player.PlayerName, WeaponManager.player.PlayerFaction);
            //设置子弹初速度
            float verticalSpeed = Vector3.Dot(bullet.transform.forward, Vector3.up) * RuntimeFireConfig.BulletSpeed;
            Vector3 horizontalVelocity = bullet.transform.forward * RuntimeFireConfig.BulletSpeed - Vector3.up * verticalSpeed;
            bullet.GetComponent<Bullet>().SetUpBulletSpeed(verticalSpeed, horizontalVelocity);
        }

    }
}